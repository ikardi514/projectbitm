<?php

class View
{
    public function __construct()
	{
        $host_name = 'localhost';
        $user_name = 'root';
        $passowrd = '';
        $db_name = 'coaching_managemnt_basis';
        $db_connect = mysqli_connect($host_name, $user_name, $passowrd);
        if ($db_connect){
            //echo 'Database server connected';
            $db_select = mysqli_select_db($db_connect, $db_name);
            if ($db_select) {
                //echo 'Database selected';
                return $db_connect;
            } else {
                die('Selection Fail' . mysqli_error($db_connect));
            }
        } else {
            die('Connection Fail' . mysqli_error($db_connect));
        }
	}
	
	public function select_all_student()
	{
		$db_connect=$this->__construct();
		$sql="SELECT s.*, b.batch_name FROM tbl_student as s, tbl_batch as b WHERE s.batch_id=b.batch_id AND s.deletion_status=1";
		if(mysqli_query($db_connect, $sql)){
            $query_result = mysqli_query($db_connect, $sql);
            return $query_result;
        }
		else{
            die('Query problem' . mysqli_error($db_connect));
        }
	}
	
	public function select_all_batch()
	{
		$db_connect=$this->__construct();
		$sql="SELECT * FROM tbl_batch WHERE deletion_status=1";
		if(mysqli_query($db_connect, $sql)){
            $query_result = mysqli_query($db_connect, $sql);
            return $query_result;
        }
		else{
            die('Query problem' . mysqli_error($db_connect));
        }
	}
	
	public function select_recent_news()
	{
		$db_connect=$this->__construct();
		$sql="SELECT * FROM tbl_news WHERE deletion_status=1 ORDER BY news_id DESC LIMIT 3";
		if(mysqli_query($db_connect, $sql)){
            $query_result = mysqli_query($db_connect, $sql);
            return $query_result;
        }
		else{
            die('Query problem' . mysqli_error($db_connect));
        }
	}
	
	public function select_all_news_limit()
	{
		$db_connect=$this->__construct();
		$count=0;
		if(isset($_GET['count']))
		{
			$count=$_GET['count'];
			if($count == 1) {
				$count =0;
			}
			else {
				$count=(($count*3)-3);
			}
		}
		$sql="SELECT * FROM tbl_news WHERE deletion_status=1 ORDER BY news_id DESC LIMIT $count, 3";
		if(mysqli_query($db_connect, $sql)){
            $query_result = mysqli_query($db_connect, $sql);
            return $query_result;
        }
		else{
            die('Query problem' . mysqli_error($db_connect));
        }
	}
	
	public function select_all_news()
	{
		$db_connect=$this->__construct();
		$sql="SELECT * FROM tbl_news WHERE deletion_status=1";
		if(mysqli_query($db_connect, $sql)){
            $query_result = mysqli_query($db_connect, $sql);
            return $query_result;
        }
		else{
            die('Query problem' . mysqli_error($db_connect));
        }
	}
	
	public function select_all_news_by_category($news_id)
	{
		$db_connect=$this->__construct();
		$sql="SELECT n.*, c.news_cat_name FROM tbl_news as n, tbl_news_category as c WHERE n.news_cat_id=c.news_cat_id AND  n.deletion_status=1 AND n.news_cat_id='$news_id'";
		if(mysqli_query($db_connect, $sql)){
            $query_result = mysqli_query($db_connect, $sql);
            return $query_result;
        }
		else{
            die('Query problem' . mysqli_error($db_connect));
        }
	}
	
	public function select_news_info_by_news_id($news_id)
	{
		$db_connect=$this->__construct();
		$sql="SELECT * FROM tbl_news WHERE news_id='$news_id'";
		if(mysqli_query($db_connect, $sql)){
            $query_result = mysqli_query($db_connect, $sql);
            return $query_result;
        }
		else{
            die('Query problem' . mysqli_error($db_connect));
        }
	}
	
	public function select_all_news_category()
	{
		$db_connect=$this->__construct();
		$sql="SELECT * FROM tbl_news_category WHERE deletion_status=1";
		if(mysqli_query($db_connect, $sql)){
            $query_result = mysqli_query($db_connect, $sql);
            return $query_result;
        }
		else{
            die('Query problem' . mysqli_error($db_connect));
        }
	}
	
	public function save_pending_student_info($data)
	{
		$db_connect=$this->__construct();
		$directory='./admin/uploaded_image/';
		$target_file=$directory.$_FILES['image']['name'];
		$file_type=pathinfo($target_file, PATHINFO_EXTENSION);
		$file_size=$_FILES['image']['size'];
		$check=getimagesize($_FILES['image']['tmp_name']);
		if($check) {
			if(file_exists($target_file)) {
				echo 'This file name is already exists. please try new one';
			} else {
				if($file_size>10000000) {
					echo 'File is too large. please try new one';
				} else {
					if($file_type !='jpg' && $file_type != 'png') {
						echo 'File type is not valid. please try new one';
					}else
					{
						move_uploaded_file($_FILES['image']['tmp_name'], $target_file);
						$query="INSERT INTO tbl_student_pending(stu_name,interest_class,father_name,mother_name,contact,email,address,gender,dob,image,current_institute,doc) VALUES('$data[stu_name]', '$data[interest_class]', '$data[father_name]', '$data[mother_name]', '$data[contact]', '$data[email]', '$data[address]', '$data[gender]', '$data[dob]','$target_file', '$data[current_institute]', '$data[doc]')";
						if (mysqli_query($db_connect, $query)){
							$message="Thanks for Registration. We will contact with you very soon";
							return $message;
						}
						else{
							die('Query Problem' . mysqli_error($db_connect));
						}
					}
				}
			}
		} 
		else{
		   $message= "This is not an image";
		   return $message;
		}
	}
	
	public function save_contact_info($data)
	{
        $db_connect = $this->__construct();
        $sql="INSERT INTO tbl_contact (name, email, subject, message) VALUES ('$data[name]', '$data[email]', '$data[subject]', '$data[message]')";
        if(mysqli_query($db_connect, $sql)){
            $message="Your message has been sent successfully";
            return $message;
        }
		else
		{
            die('Query problem' . mysqli_error($db_connect));
        }
    }
	
	public function select_all_mails()
	{
		$db_connect=$this->__construct();
		$sql="SELECT * FROM tbl_contact WHERE acceptable_status=2 ORDER BY cnt_id DESC LIMIT 5";
		if(mysqli_query($db_connect, $sql)){
            $query_result = mysqli_query($db_connect, $sql);
            return $query_result;
        }
		else{
            die('Query problem' . mysqli_error($db_connect));
        }
	}
	
	public function select_all_teacher()
	{
		$db_connect=$this->__construct();
		$sql="SELECT t.*, d.dept_name FROM tbl_teacher as t,tbl_dept as d WHERE t.dept_id=d.dept_id AND t.deletion_status=1";
		if(mysqli_query($db_connect, $sql)){
            $query_result = mysqli_query($db_connect, $sql);
            return $query_result;
        }
		else{
            die('Query problem' . mysqli_error($db_connect));
        }
	}
	
	public function select_news_for_gallery()
	{
		$db_connect=$this->__construct();
		$sql="SELECT * FROM tbl_news WHERE deletion_status=1 AND news_cat_id=8 ORDER BY news_id DESC LIMIT 1";
		if(mysqli_query($db_connect, $sql)){
            $query_result = mysqli_query($db_connect, $sql);
            return $query_result;
        }
		else{
            die('Query problem' . mysqli_error($db_connect));
        }
	}
}