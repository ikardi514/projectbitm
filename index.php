<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<?php
	ob_start();

	require './application.php';
	$obj_view=new View();  
?>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>ASOS Coaching Center</title>
	<meta name="keywords" content="" />
	<meta name="description" content="" />
	<link href="tooplate_style.css" rel="stylesheet" type="text/css" />
	<script type="text/JavaScript" src="js/jquery-1.6.3.js"></script> 
	<link rel="stylesheet" href="css/slimbox2.css" type="text/css" media="screen" /> 
	<script type="text/JavaScript" src="js/slimbox2.js"></script> 
	<link rel="stylesheet" href="css/nivo-slider.css" type="text/css" media="screen" />
	</head>
	<body>
		<?php include'./include/header.php'; ?>
		<?php
			if(isset($pages)){
				if($pages=='stu_info'){
					include'./pages/stu_info_content.php';
				}
				if($pages=='batch_info'){
					include'./pages/batch_info_content.php';
				}
				if($pages=='about'){
					include'./pages/about_content.php';
				}
				else if($pages=='contact'){
					include'./pages/contact_content.php';
				}
				else if($pages=='gallery'){
					include'./pages/gallery_content.php';
				}
				else if($pages=='news'){
					include'./pages/news_content.php';
				}
				else if($pages=='news_category'){
					include'./pages/news_category_content.php';
				}
				else if($pages=='registration'){
					include'./pages/registration_content.php';
				}
				else if($pages=='view_full_news_info'){
					include'./pages/view_full_news_info_content.php';
				}
			}
			else{
				include'./pages/home_content.php';
			}
		?>
		
		<?php include'./include/footer.php';?>
	</body>
</html>