<?php
	$news_id=$_GET['news_id'];
	$query_result=$obj_view->select_all_news_by_category($news_id);
?>
<div id="tooplate_main">
	<div id="tooplate_content" class="left">
	<?php 
		if($news_info=mysqli_fetch_assoc($query_result)) 
		{ 
	?>
		<div class="post">
			<h2><?php echo $news_info['news_title']; ?></h2>
			
			<div class="post_meta col_4">
				<span class="post_author"><p>ASOS News</p></span>
				<span class="date"><p><?php echo $news_info['doc'] ;?></p></span>
			</div>
			<div class="col_2 no_margin_right">
				<div class="img_border img_border_m img_nof">
					<img src="./admin/<?php echo $news_info['image'];?>" alt="Post Image" width="410px" height="200px"/>	
				</div>
				<strong><p><?php echo $news_info['news_title']; ?></p></strong>
				<a class="more" href="view_full_news_info.php?id=<?php echo $news_info['news_id']; ?>">More</a>
			</div>
			<div class="clear"></div>
		</div>
<?php  
		}else{
			echo "There is no news";
		}
?>
	</div> <!-- END of tooplate_content -->	
	
	
	<div id="tooplate_sidebar" class="right">
			
			<div class="content_wrapper content_mb_60">
                <h3>News Categories</h3>
				<?php 
					$query_result = $obj_view->select_all_news_category();
					while ($news_cat_info=mysqli_fetch_assoc($query_result))
					{ 
				?>
                <ul class="sidebar_link">
                    <li><a href="news_category.php?news_id=<?php echo $news_cat_info['news_cat_id']; ?>"><?php echo $news_cat_info['news_cat_name']; ?></a></li>
                </ul>
			  <?php } ?>
			</div>
            
           <div class="content_wrapper content_mb_60">
                <h3>Visitor's Email</h3>
				<?php
				$query=$obj_view->select_all_mails();
				while($contact_info=mysqli_fetch_assoc($query))
				{
				?>
                <ul class="comment_list">
                    <li>
                    	<span><?php echo $contact_info['message'];?></span>
                        <span class="comment_meta">
		                    <strong><?php echo $contact_info['name'];?></strong> - <a href="#"><?php echo $contact_info['subject'];?></a>
						</span>
					</li>
                </ul>
				<?php
				} 
				?>
            </div>
	  </div>
	
	<div class="clear"></div>
    <div style="display:none;" class="nav_up" id="nav_up"></div>
</div>