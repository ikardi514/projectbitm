<?php
	if (isset($_POST['btn'])) 
	{
		$message = $obj_view->save_pending_student_info($_POST);
	}
?>

<div id="tooplate_main">
	<h2>Registration Form</h2>
	<strong><p>
		<?php
			if(isset($message)){
				echo $message;
				unset ($message);
			}
		?>
	</p></strong>
	<div class="content_wrapper content_mb_60">
		<div id="contact_form" class="col_2">    	
			 <form method="post" name="contact" action="" enctype="multipart/form-data" >
			 
				<div class="clear"></div>
				<label for="fullname">Full Name:</label>
				<input type="text" id="fullname" name="stu_name" required class="required input_field" />
				
				<div class="clear"></div>
				<label for="fullname">Interested Class:</label>
				<input type="text" id="fullname" name="interest_class" required class="required input_field" />
				
				<div class="clear"></div>
				<label for="fullname">Father Name:</label>
				<input type="text" id="fullname" name="father_name" required class="required input_field" />
				
				<div class="clear"></div>
				<label for="fullname">Mother Name:</label>
				<input type="text" id="fullname" name="mother_name" required class="required input_field" />
				
				<div class="clear"></div>
				<label for="fullname">Contact Number:</label>
				<input type="number" id="fullname" name="contact" required class="required input_field" />
				
				<div class="clear"></div>
				<label for="subject">Email:</label>
				<input type="email" id="email" name="email" class="validate-email required input_field" />
				
			   <div class="clear"></div>
			   <label for="message">Address:</label>
			   <textarea id="message" name="address" required rows="0" cols="0" class="required"></textarea>
			   
				<div class="clear"></div>
				<label for="fullname">Gender:</label>
				<select id="fullname" name="gender" required class="required input_field">
					<option>--Select Gender--</option>
					<option value="1">Male</option>
					<option value="2">Female</option>
				</select>
				
				<div class="clear"></div>
				<label for="fullname">Date of Birth:</label>
				<input type="date" id="fullname" name="dob" required class="required input_field" />
				
				<div class="clear"></div>
				<label for="fullname">Image:</label>
				<input type="file" name="image" class="required input_field" />
				
				<div class="clear"></div>
				<label for="fullname">Current Institute:</label>
				<input type="text" name="current_institute" required class="required input_field" />
				
				<div class="clear"></div>
				<label for="fullname">Submittion Date:</label>
				<input type="date" name="doc" required class="required input_field" />
			   
				<div class="clear"></div>
				<input type="submit" name="btn" value="Submit" class="more right" />
				
			</form>
		</div>
		<div class="col_2 no_margin_right">
            <h2>Our Class</h2>
			<div class="img_border img_border_m">
				<iframe width="420" height="340" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="images/image_1.jpg"></iframe>
			</div>
		</div>
        <div class="clear"></div>
	</div>
	<div class="clear"></div>
	<div style="display:none;" class="nav_up" id="nav_up"></div>
</div>