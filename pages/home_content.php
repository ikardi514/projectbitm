<div id="tooplate_main">
	<div class="content_wrapper">
		<div class="service_list col_4">
			<center><a href="stu_info.php"><img src="images/stuinfo.png" alt="Icon" width="150px" height="150px"/>Student Information</a></center>
			<div class="clear"></div>
		</div>
		<div class="service_list col_4">
			<center><a href="batch_info.php"><img src="images/batch.png" alt="Icon" width="150px" height="150px"/>Batch Information</a></center>
			<div class="clear"></div>
		</div>
	</div>
	
	<?php
		$query_result=$obj_view->select_recent_news();
	?>
	
	<div class="content_wrapper content_mb_60">
        	<h2>Recent News</h2>
			<?php 
			while ($news_info=mysqli_fetch_assoc($query_result)) 
			{ 
			?>
        	<div class="col_3 no_margin_right_2">
            	<div class="img_border img_border_s img_nof">
                	<img src="./admin/<?php echo $news_info['image']; ?>" alt="Image" width="250px" height="125px"/>
                </div>
                <a href="view_full_news_info.php?id=<?php echo $news_info['news_id']; ?>"><strong><?php echo $news_info['news_title'] ; ?></strong></a>
            </div>
	  <?php }?>
    </div>
	
	<div class="clear"></div>
	<div style="display:none;" class="nav_up" id="nav_up"></div>
</div>