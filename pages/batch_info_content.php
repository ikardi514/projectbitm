<?php
	$query=$obj_view->select_all_batch();
?>
<head>
	<link href="style.css" rel="stylesheet" type="text/css" />
</head>
<div id="tooplate_main">
	<h2>Batch Information</h2>
	
	<?php while($batch_info=mysqli_fetch_assoc($query)){?>
	<div class="whole">
		<div class="type standard">
			<p><?php echo $batch_info['batch_name']; ?></p>
		</div>
		<div class="plan">
			<div class="content">
				<ul>
					<li>Class Name: <?php echo $batch_info['class_name']; ?></li>
					<li>Starting Time: <?php echo $batch_info['starting_time']; ?></li>
					<li>Ending Time: <?php echo $batch_info['ending_time']; ?></li>
				</ul>
			</div>
		</div>
	</div>
	<?php }?>
</div>