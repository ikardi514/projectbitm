<?php
	$query_result=$obj_view->select_all_news();
	
	$news_id=$_GET['id'];
	//echo $news_id;
	$query_result=$obj_view->select_news_info_by_news_id($news_id);
	$news_info=mysqli_fetch_assoc($query_result);
	/*echo'<pre>';
	print_r($news_info);
	echo'</pre>';*/
?>

<div id="tooplate_main">
    	
	<div id="tooplate_content" class="left">
        <div class="post">
			<h2><?php echo $news_info['news_title'];?></h2>
			<div class="post_meta col_4">
				<span class="post_author"><p>ASOS News</p></span>
				<span class="date"><p><?php echo $news_info['doc'];?></p></span>
			</div> 
			<div class="col_2 no_margin_right">
				<div class="img_border img_border_m img_nof">
					<img src="./admin/<?php echo $news_info['image'];?>" alt="Post Image" width="420px" height="200px"/>	
				</div>
			</div>
			<p><?php echo $news_info['news_details'];?></p>
		</div>
	</div>
	<div id="tooplate_sidebar" class="right">
			
			<div class="content_wrapper content_mb_60">
                <h3>News Categories</h3>
				<?php 
					$query_result = $obj_view->select_all_news_category();
					while ($news_cat_info=mysqli_fetch_assoc($query_result))
					{ 
				?>
                <ul class="sidebar_link">
                    <li><a href="#"><?php echo $news_cat_info['news_cat_name']; ?></a></li>
                </ul>
			  <?php } ?>
			</div>
	  </div>
</div>