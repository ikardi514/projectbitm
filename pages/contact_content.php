<?php
	if (isset($_POST['btn'])) 
	{
		$message = $obj_view->save_contact_info($_POST);
	}
?>

<div id="tooplate_main">
	<h2>Send a message</h2>
	<strong><p>
		<?php
			if(isset($message)){
				echo $message;
				unset ($message);
			}
		?>
	</p></strong>
	<div class="content_wrapper content_mb_60">
		<div id="contact_form" class="col_2">    	
			 <form method="post" name="contact" action="" >
			 
				<div class="clear"></div>
				<label for="fullname">Name:</label>
				<input type="text" id="fullname" name="name" class="required input_field" />
				
				<div class="clear"></div>
				<label for="subject">Email:</label>
				<input type="text" id="email" name="email" class="validate-email required input_field" />
			   
				<div class="clear"></div>
				<label for="subject">Subject:</label>
				<input type="text" id="subject" name="subject" class="validate-email required input_field" />
				
			    <div class="clear"></div>
				<label for="message">Message:</label>
			    <textarea id="message" name="message" rows="0" cols="0" class="required"></textarea>
			   
				<div class="clear"></div>
				<input type="submit" name="btn" value="Submit" class="more right" />
				
			</form>
		</div>
		<div class="col_2 no_margin_right">
            <h2>Our Location</h2>
			<div class="img_border img_border_m">
				<iframe width="420" height="340" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3619.106268474329!2d91.8651060144114!3d24.894356550030448!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0000000000000000%3A0xbfd4c66c02e7ba46!2sWest+World+Shopping+City!5e0!3m2!1sen!2sbd!4v1461588557104"></iframe>
			</div>
		</div>
        <div class="clear"></div>
	</div>
	
	<div class="content_wrapper content_mb_60">
		<div class="col_3">
			<h3>Sylhet Office</h3>
			West World Shopping City,<br />
			Jallarpar Road, Zindabazar<br />
			Sylhet - 3100, Bangladesh<br /><br />
			
			Cell: +880 1722 313843<br />
			Email: asos_2016@gmail.com
		</div>
		<div class="col_3">
			<h3>Dhaka Office</h3>
			House No-47, Road No-05<br />
			Mirpur-10,<br />
			Dhaka, Bangladesh<br /><br />
			
			Cell: +880 1722 313843<br />
			Email: asos_2016@gmail.com<br /><br />
		</div>
		<div class="col_3 no_margin_right">
			<h3>Chittagong Office</h3>
			Makka Shopping Center<br />
			29/A, Terry Bazar Rd<br />
			Chittagong, Bangladesh<br /><br />
			
			Cell: +880 1722 313843<br />
			Email: asos_2016@gmail.com 
		</div>
	</div>
	<div class="clear"></div>
	<div style="display:none;" class="nav_up" id="nav_up"></div>
</div>