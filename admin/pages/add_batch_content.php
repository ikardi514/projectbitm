<?php
	if(isset($_POST['btn'])){
		$message=$obj_admin->save_batch_info($_POST);
	}
?>

<div class="row-fluid sortable">
    <div class="box span12">
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon edit"></i><span class="break"></span>Add Batch Information Form</h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
            </div>
        </div>
        <div class="box-content">
            <h3 style="color:red;">
                <?php
                    if(isset($message)) 
					{
                        echo $message;
                        unset($message);
                    }
                ?>
                
            </h3>
            <form class="form-horizontal" action="" method="post">
                <fieldset>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Batch Name</label>
                        <div class="controls">
                            <input type="text" name="batch_name" required class="span6 typeahead" id="typeahead">
                        </div>
                    </div>
					<div class="control-group">
                        <label class="control-label" for="typeahead">Class Name</label>
                        <div class="controls">
                            <input type="text" name="class_name" required class="span6 typeahead" id="typeahead">
                        </div>
                    </div>
					<div class="control-group">
                        <label class="control-label" for="typeahead">Starting Time</label>
                        <div class="controls">
                            <input type="text" name="starting_time" required class="span6 typeahead" id="typeahead">
                        </div>
                    </div>
					<div class="control-group">
                        <label class="control-label" for="typeahead">Ending Time</label>
                        <div class="controls">
                            <input type="text" name="ending_time" required class="span6 typeahead" id="typeahead">
                        </div>
                    </div>
					<div class="control-group">
						<label class="control-label" for="date01">Publication Date</label>
						<div class="controls">
							<input type="text" name="doc" class="input-xlarge datepicker" id="date01" value="04/23/16">
						</div>
					</div>
					<div class="control-group">
                        <label class="control-label" for="selectError3">Publication Status</label>
                        <div class="controls">
                            <select id="selectError3" name="publication_status">
                                <option>--Select Publication Status--</option>
                                <option value="1">Published</option>
                                <option value="2">Unpublished</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="submit" name="btn" class="btn btn-primary">Create Batch</button>
                        <button type="reset" class="btn">Reset</button>
                    </div>
                </fieldset>
            </form>   
        </div>
    </div><!--/span-->
</div><!--/row-->