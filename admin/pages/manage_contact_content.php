<?php
	if (isset($_GET['name'])) {
		$id = $_GET['id'];
		$message = $obj_admin->delete_contact($id);
	}

	$query_result = $obj_admin->select_all_mails();
?>
<div class="row-fluid sortable">		
    <div class="box span12">
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon user"></i><span class="break"></span>Manage Emails</h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
            </div>
        </div>
        <div class="box-content">
            <?php
            if (isset($message)) {
                echo $message;
            }
            ?>
            <?php
            if (isset($_SESSION['message'])) {
                echo $_SESSION['message'];
                unset($_SESSION['message']);
            }
            ?>
            <table class="table table-striped table-bordered bootstrap-datatable datatable">
                <thead>
                    <tr>
                        <th>Sender Name</th>
						<th>Email</th>
                        <th>Subject</th>
                        <th>Message</th>
                        <th>Acceptable Status</th>
                        <th>Actions</th>
                    </tr>
                </thead>   
                <tbody>
                    <?php 
						while ($contact_info=mysqli_fetch_assoc($query_result)) 
						{ 
					?>
                        <tr>
                            <td class="center"> <?php echo $contact_info['name']; ?></td>
                            <td class="center"> <?php echo $contact_info['email']; ?></td>
							<td class="center"> <?php echo $contact_info['subject']; ?></td>
							<td class="center"> <?php echo $contact_info['message']; ?></td>
                            <td class="center">
                               <span class="label label-success">
									<?php
										if ($contact_info['acceptable_status'] == 1) {
											echo $contact_info['acceptable_status'] = 'Pending';
										} else {
											echo $contact_info['acceptable_status'] = 'Make As Read';
										}
									?>
                               </span>
                            </td>
                            <td class="center">
                                <a class="btn btn-info" href="update_contact_info.php?id=<?php echo $contact_info['cnt_id']; ?>">
                                    <i class="halflings-icon white edit"></i>  
                                </a>
                                <a class="btn btn-danger" href="?name=delete&id=<?php echo $contact_info['cnt_id']; ?>" onclick=" return checkDelete();">
                                    <i class="halflings-icon white trash"></i> 
                                </a>
                            </td>
                        </tr>
					<?php 
						} 
					?>
                </tbody>
            </table>            
        </div>
    </div><!--/span-->
</div><!--/row-->