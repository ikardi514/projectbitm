<?php
	if (isset($_POST['btn'])) 
	{
		$message = $obj_admin->save_news($_POST);
	}
?>
<div class="row-fluid sortable">
    <div class="box span12">
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon edit"></i><span class="break"></span>Add News</h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
            </div>
        </div>
        <div class="box-content">
            <h3>
                <?php
                if (isset($message)) {
                    echo $message;
                    unset($message);
                }
                ?>

            </h3>
            <form class="form-horizontal" action="" method="post" enctype="multipart/form-data">
                <fieldset>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">News Title</label>
                        <div class="controls">
                            <input type="text" name="news_title" required class="span6 typeahead" id="typeahead">
                        </div>
                    </div>
					<div class="control-group">
                        <label class="control-label" for="selectError3">News Category</label>
                        <div class="controls">
                            <select id="selectError3" name="news_cat_id">
                                <option>--Select News Category--</option>
                                <?php 
									$query_result = $obj_admin->select_all_news_category();
									while ($news_cat_info=mysqli_fetch_assoc($query_result))
									{ 
								?>
										<option value="<?php echo $news_cat_info['news_cat_id']; ?>"><?php echo $news_cat_info['news_cat_name']; ?></option>
										
                              <?php } ?>
                            </select>
                        </div>
                    </div>
					<div class="control-group">
                        <label class="control-label" for="typeahead">News Image</label>
                        <div class="controls">
                            <input type="file" name="image" required class="span6 typeahead" id="typeahead">
                        </div>
                    </div>
                    <div class="control-group hidden-phone">
                        <label class="control-label" for="textarea2">News Details</label>
                        <div class="controls">
                            <textarea name="news_details" class="cleditor" id="textarea2" rows="3"></textarea>
                        </div>
                    </div>
					<div class="control-group">
						<label class="control-label" for="date01">Publication Date</label>
						<div class="controls">
							<input type="text" name="doc" class="input-xlarge datepicker" id="date01" value="04/16/16">
						</div>
					</div>
                    <div class="control-group">
                        <label class="control-label" for="selectError3">Publication Status</label>
                        <div class="controls">
                            <select id="selectError3" name="publication_status">
                                <option>--Select Publication Status--</option>
                                <option value="1">Published</option>
                                <option value="2">Unpublished</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="submit" name="btn" class="btn btn-primary">Publish News</button>
                        <button type="reset" class="btn">Reset</button>
                    </div>
                </fieldset>
            </form>   
        </div>
    </div><!--/span-->
</div><!--/row-->