<?php
	$tea_id=$_GET['tea_id'];
	//echo $tea_id;
	$query_result=$obj_admin->select_teacher_info_by_tea_id($tea_id);
	$teacher_info=mysqli_fetch_assoc($query_result);
	/*echo'<pre>';
	print_r($teacher_info);
	echo'</pre>';*/
?>

<div class="row">
	<div class="well">
		<h3>Teacher's Full Information</h3>
		<table class="table table-bordered">
			<tr>
				<td><img src="<?php echo $teacher_info['image'];?> " alt="image" width="120px" height="120px" style="border-radius:100px; "/></td>
			</tr>
			<tr>
				<th><h3 style="color:green; "><?php echo $teacher_info['tea_name'];?></h3></th>
			</tr>
			<tr>
				<th>Teacher ID</th>
				<td style="color:red; "><?php echo $teacher_info['tea_office_id'];?></td>
			</tr>
			<tr>
				<th>Department</th>
				<td><?php echo $teacher_info['dept_name'];?></td>
			</tr>
			<tr>
				<th>Joining Date</th>
				<td><?php echo $teacher_info['doc'];?></td>
			</tr>
			<tr>
				<th>Profile Last Modified</th>
				<td><?php echo $teacher_info['dom'];?></td>
			</tr>
			<tr>
				<th>Gender</th>
				<td>
					<?php
						if($teacher_info['gender']==1){
							echo $teacher_info['gender']= 'Male';
						}else{
							echo $teacher_info['gender']= 'Female';
						}
						
					?>
				</td>
			</tr>
			<tr>
				<th>Date of Birth</th>
				<td><?php echo $teacher_info['dob'];?></td>
			</tr>
		</table>
	</div>
</div>

<div class="row">
	<div class="well">
		<h3>Contact Information</h3>
		<table class="table table-bordered">
			<tr>
				<th>Contact</th>
				<td><?php echo $teacher_info['contact'];?></td>
			</tr>
			<tr>
				<th>Email</th>
				<td><?php echo $teacher_info['email'];?></td>
			</tr>
			<tr>
				<th>Address</th>
				<td><?php echo $teacher_info['address'];?></td>
			</tr>
		</table>
	</div>
</div>

<div class="row">
	<div class="well">
		<h3>Personal Information</h3>
		<table class="table table-bordered">
			<tr>
				<th>Father's Name</th>
				<td><?php echo $teacher_info['father_name'];?></td>
			</tr>
			<tr>
				<th>Mother's Name</th>
				<td><?php echo $teacher_info['mother_name'];?></td>
			</tr>
		</table>
	</div>
</div>