<?php
	$dept_id = $_GET['id'];
	$query_result=$obj_admin->show_dept_info($dept_id);
	$result_f = mysqli_fetch_assoc($query_result);
	//echo '<pre>';
	//print_r($result_f);
	//exit();

	if(isset($_POST['btn'])){
		$message=$obj_admin->update_dept_info($_POST);  
	}
?>
<div class="row-fluid sortable">
    <div class="box span12">
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon edit"></i><span class="break"></span>Update Department Information</h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
            </div>
        </div>
        <div class="box-content">
            <form name="edit_admin_form" class="form-horizontal" action="" method="post">
                <fieldset>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Department Name</label>
                        <div class="controls">
                            <input type="text" name="dept_name" required class="span6 typeahead" id="typeahead" value="<?php echo $result_f['dept_name']; ?>">
                            <input type="hidden" name="dept_id" required class="span6 typeahead" id="typeahead" value="<?php echo $result_f['dept_id']; ?>">
                        </div>
                    </div>
					<div class="control-group">
                        <label class="control-label" for="typeahead">Date of Publication</label>
                        <label class="control-label" for="typeahead"><?php echo $result_f['doc']; ?></label>
                    </div>
					<div class="control-group">
                        <label class="control-label" for="typeahead">Date of Modified</label>
                        <div class="controls">
                            <input type="text" name="dom" class="input-xlarge datepicker" id="date01" value="04/23/16">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="selectError3">Publication Status</label>
                        <div class="controls">
                            <select id="selectError3" name="publication_status">
                                <option>--Select Publication Status--</option>
                                <option value="1">Published</option>
                                <option value="2">Unpublished</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="submit" name="btn" class="btn btn-primary">Update</button>
                        <button type="reset" class="btn">Reset</button>
                    </div>
                </fieldset>
            </form>   
        </div>
    </div><!--/span-->
</div><!--/row-->

<script>
    document.forms['edit_admin_form'].elements['publication_status'].value='<?php echo $result_f['publication_status']; ?>';
</script>