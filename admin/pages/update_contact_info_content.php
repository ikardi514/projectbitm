<?php
	$cnt_id=$_GET['id'];
	$query_result=$obj_admin->show_contact_info_by_id($cnt_id);
	$result_f = mysqli_fetch_assoc($query_result);
	//echo '<pre>';
	//print_r($result_f);
	//exit();

	if(isset($_POST['btn'])){
		$message=$obj_admin->update_contact_info($_POST);  
	}
?>
<div class="row-fluid sortable">
    <div class="box span12">
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon edit"></i><span class="break"></span>Update Emails</h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
            </div>
        </div>
        <div class="box-content">
            <form name="edit_admin_form" class="form-horizontal" action="" method="post" enctype="multipart/form-data">
                <fieldset>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Email Sender Name</label>
                        <label class="control-label" for="typeahead"><?php echo $result_f['name']; ?></label>
                        <div class="controls">
                            <input type="hidden" name="cnt_id" required class="span6 typeahead" id="typeahead" value="<?php echo $result_f['cnt_id']; ?>">
                        </div>
                    </div>
					<div class="control-group">
                        <label class="control-label" for="typeahead">Email</label>
						<label class="control-label" for="typeahead"><?php echo $result_f['email']; ?></label>
                    </div>
					<div class="control-group">
                        <label class="control-label" for="typeahead">Subject</label>
						<label class="control-label" for="typeahead"><?php echo $result_f['subject']; ?></label>
                    </div>
					<div class="control-group">
                        <label class="control-label" for="typeahead">Message</label>
                        <div class="controls">
                            <textarea name="message" class="cleditor" id="textarea2" rows="3"><?php echo $result_f['message']; ?></textarea>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="selectError3">Acceptable Status</label>
                        <div class="controls">
                            <select id="selectError3" name="acceptable_status">
                                <option>--Select Acceptable Status--</option>
                                <option value="1">Pending</option>
                                <option value="2">Make As Read</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="submit" name="btn" class="btn btn-primary">Update</button>
                        <button type="reset" class="btn">Reset</button>
                    </div>
                </fieldset>
            </form>   
        </div>
    </div><!--/span-->
</div><!--/row-->

<script>
    document.forms['edit_admin_form'].elements['acceptable_status'].value='<?php echo $result_f['acceptable_status']; ?>';
</script>