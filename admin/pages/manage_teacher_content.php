<?php
	if (isset($_GET['name'])){
		$id = $_GET['id'];
		$message = $obj_admin->delete_tea_info($id);
	}

	$query_result = $obj_admin->select_all_teacher();
?>
<div class="row-fluid sortable">		
    <div class="box span12">
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon user"></i><span class="break"></span>Manage Teacher Information</h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
            </div>
        </div>
        <div class="box-content">
            <?php
            if (isset($message)) {
                echo $message;
            }
            ?>
            <?php
            if (isset($_SESSION['message'])) {
                echo $_SESSION['message'];
                unset($_SESSION['message']);
            }
            ?>
            <table class="table table-striped table-bordered bootstrap-datatable datatable">
                <thead>
                    <tr>
                        <th>Teacher ID</th>
                        <th>Teacher Name</th>
						<th>Department</th>
                        <th>Contact</th>
                        <th>Email Address</th>
						<th>Address</th>
						<th>Gender</th>
                        <th>Picture</th>
                        <th>Date of Joining</th>
                        <th>Acceptable Status</th>
                        <th>Actions</th>
                    </tr>
                </thead>   
                <tbody>
                    <?php 
						while ($teacher_info=mysqli_fetch_assoc($query_result)) 
						{ 
					?>
                        <tr>
                            <td class="center"> <?php echo $teacher_info['tea_office_id']; ?></td>
                            <td class="center"> <?php echo $teacher_info['tea_name']; ?></td>
                            <td class="center"> <?php echo $teacher_info['dept_name']; ?></td>
                            <td class="center"> <?php echo $teacher_info['contact']; ?></td>
                            <td class="center"> <?php echo $teacher_info['email']; ?></td>
                            <td class="center"> <?php echo $teacher_info['address']; ?></td>
							<td class="center">
								<?php
									if ($teacher_info['gender'] == 1) {
										echo $teacher_info['gender'] = 'Male';
									} else {
										echo $teacher_info['gender'] = 'Female';
									}
								?>
                            </td>
							<td class="center">
								<img src="<?php echo $teacher_info['image']; ?>" alt="image_student" width="150px" height="150px"/> 
							</td>
                            <td class="center"> <?php echo $teacher_info['doc']; ?></td>
                            <td class="center">
                               <span class="label label-success">
									<?php
										if ($teacher_info['acceptable_status'] == 1) {
											echo $teacher_info['acceptable_status'] = 'Tutor';
										} else {
											echo $teacher_info['acceptable_status'] = 'Leaved';
										}
									?>
                               </span>
                            </td>
                            <td class="center">
								<a class="btn btn-info" href="view_teacher_info.php?tea_id=<?php echo $teacher_info['tea_id']; ?>" title="View Teacher">
                                    <i class="halflings-icon white zoom-in"></i>  
                                </a>
                                <a class="btn btn-info" href="update_teacher_info.php?id=<?php echo $teacher_info['tea_id']; ?>">
                                    <i class="halflings-icon white edit"></i>  
                                </a>
                                <a class="btn btn-danger" href="?name=delete&id=<?php echo $teacher_info['tea_id']; ?>" onclick=" return checkDelete();">
                                    <i class="halflings-icon white trash"></i> 
                                </a>
                            </td>
                        </tr>
					<?php 
						} 
					?>
                </tbody>
            </table>            
        </div>
    </div><!--/span-->
</div><!--/row-->