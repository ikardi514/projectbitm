<?php
	if (isset($_GET['name'])){
		$id = $_GET['id'];
		$message = $obj_admin->delete_stu_info($id);
	}

	$query_result = $obj_admin->select_all_pending_student();
?>
<div class="row-fluid sortable">		
    <div class="box span12">
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon user"></i><span class="break"></span>Manage Submitted Student's Information</h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
            </div>
        </div>
        <div class="box-content">
            <?php
            if (isset($message)) {
                echo $message;
            }
            ?>
            <?php
            if (isset($_SESSION['message'])) {
                echo $_SESSION['message'];
                unset($_SESSION['message']);
            }
            ?>
            <table class="table table-striped table-bordered bootstrap-datatable datatable">
                <thead>
                    <tr>
                        <th>Student Name</th>
						<th>Interest Class</th>
						<th>Father Name</th>
						<th>Mother Name</th>
                        <th>Contact</th>
                        <th>Email Address</th>
						<th>Address</th>
						<th>Date of Birth</th>
                        <th>Gender</th>
                        <th>Picture</th>
                        <th>Current Institute</th>
                        <th>Date of Submission</th>
                        <th>Actions</th>
                    </tr>
                </thead>   
                <tbody>
                    <?php 
						while ($student_info=mysqli_fetch_assoc($query_result)) 
						{ 
					?>
                        <tr>
                            <td class="center"> <?php echo $student_info['stu_name']; ?></td>
                            <td class="center"> <?php echo $student_info['interest_class']; ?></td>
                            <td class="center"> <?php echo $student_info['father_name']; ?></td>
                            <td class="center"> <?php echo $student_info['mother_name']; ?></td>
							<td class="center"> <?php echo $student_info['contact']; ?></td>
                            <td class="center"> <?php echo $student_info['email']; ?></td>
							<td class="center"> <?php echo $student_info['address']; ?></td>
							<td class="center"> <?php echo $student_info['dob']; ?></td>
							<td class="center">
								<?php
									if ($student_info['gender'] == 1) {
										echo $student_info['gender'] = 'Male';
									} else {
										echo $student_info['gender'] = 'Female';
									}
								?>
                            </td>
							<td class="center">
								<img src="../<?php echo $student_info['image']; ?>" alt="image_student" width="150px" height="150px"/> 
							</td>
							 <td class="center"> <?php echo $student_info['current_institute']; ?></td>
                            <td class="center"> <?php echo $student_info['doc']; ?></td>
                            <td class="center">
                                <a class="btn btn-info" href="update_pending_student_info.php?id=<?php echo $student_info['stu_id_pend']; ?>" title="Edit" >
                                    <i class="halflings-icon white edit"></i>  
                                </a>
                                <a class="btn btn-danger" href="?name=delete&id=<?php echo $student_info['stu_id_pend']; ?>" onclick=" return checkDelete();" title="Reject" >
                                    <i class="halflings-icon white trash"></i> 
                                </a>
                            </td>
                        </tr>
					<?php 
						} 
					?>
                </tbody>
            </table>            
        </div>
    </div><!--/span-->
</div><!--/row-->