<?php
	$stu_id=$_GET['stu_id'];
	//echo $stu_id;
	$query_result=$obj_admin->select_student_info_by_stu_id($stu_id);
	$student_info=mysqli_fetch_assoc($query_result);
	/*echo'<pre>';
	print_r($student_info);
	echo'</pre>';*/
?>

<div class="row">
	<div class="well">
		<h3>Student Full Information</h3>
		<table class="table table-bordered">
			<tr>
				<td><img src="<?php echo $student_info['image'];?> " alt="image" width="120px" height="120px" style="border-radius:100px; "/></td>
			</tr>
			<tr>
				<th><h3 style="color:green; "><?php echo $student_info['stu_name'];?></h3></th>
			</tr>
			<tr>
				<th>Student ID</th>
				<td style="color:red; "><?php echo $student_info['stu_class_id'];?></td>
			</tr>
			<tr>
				<th>Class</th>
				<td><?php echo $student_info['batch_name'];?></td>
			</tr>
			<tr>
				<th>Admission Date</th>
				<td><?php echo $student_info['doc'];?></td>
			</tr>
			<tr>
				<th>Gender</th>
				<td>
					<?php
						if($student_info['gender']==1){
							echo $student_info['gender']= 'Male';
						}else{
							echo $student_info['gender']= 'Female';
						}
						
					?>
				</td>
			</tr>
		</table>
	</div>
</div>

<div class="row">
	<div class="well">
		<h3>Contact Information</h3>
		<table class="table table-bordered">
			<tr>
				<th>Contact</th>
				<td><?php echo $student_info['contact'];?></td>
			</tr>
			<tr>
				<th>Email</th>
				<td><?php echo $student_info['email'];?></td>
			</tr>
			<tr>
				<th>Address</th>
				<td><?php echo $student_info['address'];?></td>
			</tr>
		</table>
	</div>
</div>

<div class="row">
	<div class="well">
		<h3>Personal Information</h3>
		<table class="table table-bordered">
			<tr>
				<th>Father's Name</th>
				<td><?php echo $student_info['father_name'];?></td>
			</tr>
			<tr>
				<th>Mother's Name</th>
				<td><?php echo $student_info['mother_name'];?></td>
			</tr>
		</table>
	</div>
</div>