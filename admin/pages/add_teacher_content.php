<?php
	$query_result=$obj_admin->select_all_dept_info();
	if (isset($_POST['btn']))
	{
		$message = $obj_admin->save_teacher_info($_POST);
	}
?>
<div class="row-fluid sortable">
    <div class="box span12">
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon edit"></i><span class="break"></span>Add Teacher</h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
            </div>
        </div>
        <div class="box-content">
            <h3>
                <?php
                if (isset($message)) {
                    echo $message;
                    unset($message);
                }
                ?>

            </h3>
            <form class="form-horizontal" action="" method="post" enctype="multipart/form-data">
                <fieldset>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Teacher ID</label>
                        <div class="controls">
                            <input type="text" name="tea_office_id" required class="span6 typeahead" id="typeahead"/>
                        </div>
                    </div>
					<div class="control-group">
                        <label class="control-label" for="typeahead">Teacher Name</label>
                        <div class="controls">
                            <input type="text" name="tea_name" required class="span6 typeahead" id="typeahead"/>
                        </div>
                    </div>
					<div class="control-group">
                        <label class="control-label" for="selectError3">Department</label>
                        <div class="controls">
                            <select id="selectError3" name="dept_id">
                                <option>--Select Department--</option>
                                <?php 
									while ($dept_info=mysqli_fetch_assoc($query_result))
									{ 
								?>
										<option value="<?php echo $dept_info['dept_id']; ?>"><?php echo $dept_info['dept_name']; ?></option>
										
                              <?php } ?>
                            </select>
                        </div>
                    </div>
					<div class="control-group">
                        <label class="control-label" for="typeahead">Father's Name</label>
                        <div class="controls">
                            <input type="text" name="father_name" required class="span6 typeahead" id="typeahead"/>
                        </div>
                    </div>
					<div class="control-group">
                        <label class="control-label" for="typeahead">Mother's Name</label>
                        <div class="controls">
                            <input type="text" name="mother_name" required class="span6 typeahead" id="typeahead"/>
                        </div>
                    </div>
					<div class="control-group">
                        <label class="control-label" for="typeahead">Contact Number</label>
                        <div class="controls">
                            <input type="number" name="contact" required class="span6 typeahead" id="typeahead"/>
                        </div>
                    </div>
					<div class="control-group">
                        <label class="control-label" for="typeahead">Email Address</label>
                        <div class="controls">
                            <input type="email" name="email" required class="span6 typeahead" id="typeahead"/>
                        </div>
                    </div>
					<div class="control-group hidden-phone">
                        <label class="control-label" for="textarea2">Address</label>
                        <div class="controls">
                            <textarea name="address" class="cleditor" id="textarea2" rows="3"></textarea>
                        </div>
                    </div>
					<div class="control-group">
						<label class="control-label" for="date01">Date of Birth</label>
						<div class="controls">
							<input type="text" name="dob" class="input-xlarge datepicker" id="date01" value="04/16/16">
						</div>
					</div>
					<div class="control-group">
                        <label class="control-label" for="selectError3">Gender</label>
                        <div class="controls">
                            <select id="selectError3" name="gender">
                                <option>--Select Gender--</option>
                                <option value="1">Male</option>
                                <option value="2">Female</option>
                            </select>
                        </div>
                    </div>
					<div class="control-group">
                        <label class="control-label" for="typeahead">Upload a Picture</label>
                        <div class="controls">
                            <input type="file" name="image" required class="span6 typeahead" id="typeahead">
                        </div>
                    </div>
					<div class="control-group">
						<label class="control-label" for="date01">Joining Date</label>
						<div class="controls">
							<input type="text" name="doc" class="input-xlarge datepicker" id="date02" value="01/01/16">
						</div>
					</div>
                    <div class="control-group">
                        <label class="control-label" for="selectError3">Acceptable Status</label>
                        <div class="controls">
                            <select id="selectError3" name="acceptable_status">
                                <option>--Select Acceptable Status--</option>
                                <option value="1">Join</option>
                                <option value="2">Resign</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="submit" name="btn" class="btn btn-primary">Add Teacher</button>
                        <button type="reset" class="btn">Reset</button>
                    </div>
                </fieldset>
            </form>   
        </div>
    </div><!--/span-->
</div><!--/row-->