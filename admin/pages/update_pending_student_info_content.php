<?php
	$stu_id_pend=$_GET['id'];
	$query_result=$obj_admin->select_pend_student_info_by_id($stu_id_pend);
	$result_f = mysqli_fetch_assoc($query_result);

	if(isset($_POST['btn'])){
		$message=$obj_admin->update_pending_stu_info($_POST);
		//$query=$obj_admin->delete_pending_stu_info($stu_id_pend);
		//echo '<pre>';
		//print_r($_POST);
		//exit();
	}
?>
<div class="row-fluid sortable">
    <div class="box span12">
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon edit"></i><span class="break"></span>Update Submitted Student's Information</h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
            </div>
        </div>
        <div class="box-content">
			<?php
            if (isset($message)) {
                echo $message;
            }
            ?>
            <form name="edit_admin_form" class="form-horizontal" action="" method="post" enctype="multipart/form-data">
                <fieldset>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Student Name</label>
                        <div class="controls">
                            <input type="text" name="stu_name" required class="span6 typeahead" id="typeahead" value="<?php echo $result_f['stu_name']; ?>">
							<input type="hidden" name="stu_id_pend" required class="span6 typeahead" id="typeahead" value="<?php echo $result_f['stu_id_pend']; ?>">
                        </div>
                    </div>
					<div class="control-group">
                        <label class="control-label" for="typeahead">Interest Class</label>
                        <label class="control-label" for="typeahead"><?php echo $result_f['interest_class']; ?></label>
                    </div>
					<div class="control-group">
                        <label class="control-label" for="selectError3">Batch</label>
                        <div class="controls">
                            <select id="selectError3" name="batch_id">
                                <option>--Select Batch--</option>
                                <?php
									$query_result_1=$obj_admin->select_all_batch_info_2();
									while ($batch_info=mysqli_fetch_assoc($query_result_1))
									{ 
								?>
										<option value="<?php echo $batch_info['batch_id']; ?>"><?php echo $batch_info['batch_name']; ?></option>
										
                              <?php } ?>
                            </select>
                        </div>
                    </div>
					<div class="control-group">
                        <label class="control-label" for="typeahead">Father's Name</label>
                        <div class="controls">
                            <input type="text" name="father_name" required class="span6 typeahead" id="typeahead" value="<?php echo $result_f['father_name']; ?>">
                        </div>
                    </div>
					<div class="control-group">
                        <label class="control-label" for="typeahead">Mother's Name</label>
                        <div class="controls">
                            <input type="text" name="mother_name" required class="span6 typeahead" id="typeahead" value="<?php echo $result_f['mother_name']; ?>">
                        </div>
                    </div>
					<div class="control-group">
                        <label class="control-label" for="typeahead">Contact Number</label>
                        <div class="controls">
                            <input type="text" name="contact" required class="span6 typeahead" id="typeahead" value="<?php echo $result_f['contact']; ?>">
                        </div>
                    </div>
					<div class="control-group">
                        <label class="control-label" for="typeahead">Email</label>
                        <div class="controls">
                            <input type="text" name="email" required class="span6 typeahead" id="typeahead" value="<?php echo $result_f['email']; ?>">
                        </div>
                    </div>
					<div class="control-group">
                        <label class="control-label" for="typeahead">Address</label>
                        <div class="controls">
                            <textarea name="address" class="cleditor" id="textarea2" rows="3"><?php echo $result_f['address']; ?></textarea>
                        </div>
                    </div>
					<div class="control-group">
                        <label class="control-label" for="selectError3">Gender</label>
                        <div class="controls">
                            <select id="selectError3" name="gender">
                                <option>--Select Gender--</option>
                                <option value="1">Male</option>
                                <option value="2">Female</option>
                            </select>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Image</label>
                        <div class="controls">
                            <img src="../<?php echo $result_f['image']; ?>" alt="image" width="150px" height="150px"/>
                        </div>
                    </div>
					<div class="control-group">
                        <label class="control-label" for="typeahead">Image URL</label>
                        <div class="controls">
                            <input type="text" name="image" required class="span6 typeahead" id="typeahead" value="<?php echo $result_f['image']; ?>">
                        </div>
                    </div>
					<div class="control-group">
                        <label class="control-label" for="typeahead">Give him/her ID</label>
                        <div class="controls">
                            <input type="text" name="stu_class_id" required class="span6 typeahead" id="typeahead" >
                        </div>
                    </div>
					<div class="control-group">
                        <label class="control-label" for="typeahead">Admission Date</label>
                        <div class="controls">
                            <input type="text" name="doc" required class="input-xlarge datepicker" id="date01" value="04/16/16">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="selectError3">Acceptable Status</label>
                        <div class="controls">
                            <select id="selectError3" name="acceptable_status">
                                <option>--Select Acceptable Status--</option>
                                <option value="1">Accept</option>
                                <option value="2">Reject</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="submit" name="btn" class="btn btn-primary">Update Information</button>
                    </div>
                </fieldset>
            </form>   
        </div>
    </div><!--/span-->
</div><!--/row-->

<script>
    document.forms['edit_admin_form'].elements['gender'].value='<?php echo $result_f['gender']; ?>';
</script>