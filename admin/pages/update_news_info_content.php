<?php
	$news_id=$_GET['id'];
	$query_result=$obj_admin->show_news_info($news_id);
	$result_f = mysqli_fetch_assoc($query_result);
	//echo '<pre>';
	//print_r($result_f);
	//exit();

	if(isset($_POST['btn'])){
		$message=$obj_admin->update_news_info($_POST);  
	}
?>
<div class="row-fluid sortable">
    <div class="box span12">
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon edit"></i><span class="break"></span>Update News</h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
            </div>
        </div>
        <div class="box-content">
            <form name="edit_admin_form" class="form-horizontal" action="" method="post" enctype="multipart/form-data">
                <fieldset>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">News Title</label>
                        <div class="controls">
                            <input type="text" name="news_title" required class="span6 typeahead" id="typeahead" value="<?php echo $result_f['news_title']; ?>">
                            <input type="hidden" name="news_id" required class="span6 typeahead" id="typeahead" value="<?php echo $result_f['news_id']; ?>">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Image</label>
                        <div class="controls">
                            <img src="<?php echo $result_f['image']; ?>" alt="image" width="300px" height="120px"/>
                        </div>
                    </div>
					<div class="control-group">
                        <label class="control-label" for="typeahead">Update News Image</label>
                        <div class="controls">
                            <input type="file" name="image" class="span6 typeahead" id="typeahead">
                        </div>
                    </div>
					<div class="control-group">
                        <label class="control-label" for="typeahead">Details</label>
                        <div class="controls">
                            <textarea name="news_details" class="cleditor" id="textarea2" rows="3"><?php echo $result_f['news_details']; ?></textarea>
                        </div>
                    </div>
					<div class="control-group">
                        <label class="control-label" for="typeahead">Date of Publication</label>
                        <div class="controls">
                            <input type="text" name="dom" class="span6 typeahead" id="typeahead" value="<?php echo $result_f['doc']; ?>" >
                        </div>
                    </div>
					<div class="control-group">
                        <label class="control-label" for="typeahead">Date of Modification</label>
                        <div class="controls">
                            <input type="text" name="dom" required class="input-xlarge datepicker" id="date01" value="04/16/16">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="selectError3">Publication Status</label>
                        <div class="controls">
                            <select id="selectError3" name="publication_status">
                                <option>--Select Publication Status--</option>
                                <option value="1">Published</option>
                                <option value="2">Unpublished</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="submit" name="btn" class="btn btn-primary">Update News</button>
                        <button type="reset" class="btn">Reset</button>
                    </div>
                </fieldset>
            </form>   
        </div>
    </div><!--/span-->
</div><!--/row-->

<script>
    document.forms['edit_admin_form'].elements['publication_status'].value='<?php echo $result_f['publication_status']; ?>';
</script>