<?php

class Admin
{
    public function __construct()
	{
        $host_name = 'localhost';
        $user_name = 'root';
        $passowrd = '';
        $db_name = 'coaching_managemnt_basis';
        $db_connect = mysqli_connect($host_name, $user_name, $passowrd);
        if ($db_connect){
            //echo 'Database server connected';
            $db_select = mysqli_select_db($db_connect, $db_name);
            if ($db_select) {
                //echo 'Database selected';
                return $db_connect;
            } else {
                die('Selection Fail' . mysqli_error($db_connect));
            }
        } else {
            die('Connection Fail' . mysqli_error($db_connect));
        }
    }

    public function admin_login_check($data)
	{
        $db_connect = $this->__construct();
        $password = md5($data['password']);
        $sql = "SELECT * FROM tbl_admin WHERE email_address='$data[email_address]' AND password='$password' ";
        if (mysqli_query($db_connect, $sql)){
            $query_result = mysqli_query($db_connect, $sql);
            $admin_info = mysqli_fetch_assoc($query_result);
//            echo '<pre>';
//            print_r($admin_info);
//            exit();
            if ($admin_info){
                $_SESSION['admin_id'] = $admin_info['admin_id'];
                $_SESSION['admin_name'] = $admin_info['admin_name'];
                header('Location: admin_master.php');
            } else {
                $ex_message="Please use valid email address and password";
                return $ex_message;
            }
        } else {
            die('Query problem' . mysqli_error($db_connect));
        }
    }

    public function admin_logout()
	{
        unset($_SESSION['admin_id']);
        unset($_SESSION['admin_name']);
        header('Location: index.php');
    }
	
	public function save_admin_user_info($data)
	{
        $db_connect = $this->__construct();
        $password = md5($data['password']);
        $sql="INSERT INTO tbl_admin (admin_name, email_address, password, access_level) VALUES ('$data[admin_name]', '$data[email_address]', '$password', '$data[access_level]')";
        if(mysqli_query($db_connect, $sql)){
            $message="Admin user info save successfully";
            return $message;
        }
		else
		{
            die('Query problem' . mysqli_error($db_connect));
        }
    }
	
	public function select_all_user_info()
	{
        $db_connect=$this->__construct();
        $sql="SELECT * FROM tbl_admin WHERE deletion_status=1";
        if(mysqli_query($db_connect, $sql)){
            $query_result = mysqli_query($db_connect, $sql);
            return $query_result;
        }else{
            die('Query problem' . mysqli_error($db_connect));
        }
    }
	
	public function show_admin_user_info($admin_id)
	{
		$db_connect=$this->__construct();
		$sql="SELECT * FROM tbl_admin WHERE admin_id='$admin_id' ";
		if(mysqli_query($db_connect, $sql)){
		   $query_result=mysqli_query($db_connect, $sql);
		   return $query_result;
		}else{
			die('Query problem'.  mysqli_error($db_connect) );
		}
	}
	
	public function update_admin_user_info($data)
	{
        $db_connect = $this->__construct();
        $password = md5($data['password']);
        $sql = "UPDATE tbl_admin SET admin_name='$data[admin_name]',email_address='$data[email_address]', password='$password', access_level='$data[access_level]' WHERE admin_id='$data[admin_id]'";
        if (mysqli_query($db_connect, $sql)) {
            $_SESSION['message'] = "Admin information updated successfully";
            header('Location: manage_admin.php');
        }else{
            die('Query Problem' . mysqli_error($db_connect));
        }
    }
	
	public function delete_data($id)
	{
        $db_connect = $this->__construct();
		//$sql = "DELETE FROM tbl_admin WHERE admin_id='$id'";
        $sql="UPDATE tbl_admin SET deletion_status=0 WHERE admin_id='$id' ";
        if (mysqli_query($db_connect, $sql)) {
            $message="Delete Successfully";
            return $message;
        } else {
            die('Query Problem' . mysqli_error($db_connect));
        }
    }
	
	public function save_batch_info($data)
	{
		$db_connect=$this->__construct();
		//$batch_name='.$batch_name.' '.$session.' '.$section.';
		//$sql = 'SELECT batch_id FROM tbl_batch WHERE batch_name = "'.$data[batch_name].'" AND session = "'.$data[session].'" AND section = "'.$data[section].'"';
		$sql = 'SELECT batch_id FROM tbl_batch WHERE batch_name = "'.$data[batch_name].'" ';
		if(mysqli_num_rows(mysqli_query($db_connect, $sql)) == 0)
		{
			$query="INSERT INTO tbl_batch(batch_name,class_name,starting_time,ending_time,doc,publication_status) VALUES('$data[batch_name]', '$data[class_name]', '$data[starting_time]', '$data[ending_time]', '$data[doc]', '$data[publication_status]')";
			if (mysqli_query($db_connect, $query)) {
				$message="Create Batch Successfully";
				return $message;
			}
			else {
				die('Query Problem' . mysqli_error($db_connect));
			}
		}
		else{
			$message="Already exist. Please try another one";
			return $message;
		}
	}
	
	public function select_all_batch_info()
	{
		$db_connect=$this->__construct();
		$sql="SELECT * FROM tbl_batch WHERE deletion_status=1";
		if(mysqli_query($db_connect, $sql)){
            $query_result = mysqli_query($db_connect, $sql);
            return $query_result;
        }
		else{
            die('Query problem' . mysqli_error($db_connect));
        }
	}
	
	public function show_batch_info($batch_id)
	{
		$db_connect=$this->__construct();
		$sql="SELECT * FROM tbl_batch WHERE batch_id='$batch_id' ";
		if(mysqli_query($db_connect, $sql)){
		   $query_result=mysqli_query($db_connect, $sql);
		   return $query_result;
		}else{
			die('Query problem'.  mysqli_error($db_connect) );
		}
	}
	
	public function update_batch_info($data)
	{
        $db_connect = $this->__construct();
        $sql = "UPDATE tbl_batch SET batch_name='$data[batch_name]', class_name='$data[class_name]', starting_time='$data[starting_time]', ending_time='$data[ending_time]', publication_status='$data[publication_status]' WHERE batch_id='$data[batch_id]'";
        if (mysqli_query($db_connect, $sql)) {
            $_SESSION['message'] = "Batch information updated successfully";
            header('Location: manage_batch.php');
        }else{
            die('Query Problem' . mysqli_error($db_connect));
        }
    }
	
	public function delete_batch_data($id)
	{
        $db_connect = $this->__construct();
		//$sql = "DELETE FROM tbl_batch WHERE batch_id='$id'";
        $sql="UPDATE tbl_batch SET deletion_status=0 WHERE batch_id='$id' ";
        if (mysqli_query($db_connect, $sql)) {
            $message="Delete Successfully";
            return $message;
        } else {
            die('Query Problem' . mysqli_error($db_connect));
        }
    }
	
	public function save_dept_info($data)
	{
		$db_connect=$this->__construct();
		//$batch_name='.$batch_name.' '.$session.' '.$section.';
		//$sql = 'SELECT batch_id FROM tbl_batch WHERE batch_name = "'.$data[batch_name].'" AND session = "'.$data[session].'" AND section = "'.$data[section].'"';
		$sql = 'SELECT dept_id FROM tbl_dept WHERE dept_name = "'.$data[dept_name].'" ';
		if(mysqli_num_rows(mysqli_query($db_connect, $sql)) == 0)
		{
			$query="INSERT INTO tbl_dept(dept_name,doc,dom,publication_status) VALUES('$data[dept_name]', '$data[doc]', '$data[doc]', '$data[publication_status]')";
			if (mysqli_query($db_connect, $query)) {
				$message="Create Department Successfully";
				return $message;
			}
			else {
				die('Query Problem' . mysqli_error($db_connect));
			}
		}
		else{
			$message="Already exist. Please try another one";
			return $message;
		}
	}
	
	public function select_all_dept_info()
	{
		$db_connect=$this->__construct();
		$sql="SELECT * FROM tbl_dept WHERE deletion_status=1";
		if(mysqli_query($db_connect, $sql)){
            $query_result = mysqli_query($db_connect, $sql);
            return $query_result;
        }
		else{
            die('Query problem' . mysqli_error($db_connect));
        }
	}
	
	public function show_dept_info($dept_id)
	{
		$db_connect=$this->__construct();
		$sql="SELECT * FROM tbl_dept WHERE dept_id='$dept_id' ";
		if(mysqli_query($db_connect, $sql)){
		   $query_result=mysqli_query($db_connect, $sql);
		   return $query_result;
		}else{
			die('Query problem'.  mysqli_error($db_connect) );
		}
	}
	
	public function update_dept_info($data)
	{
        $db_connect = $this->__construct();
        $sql = "UPDATE tbl_dept SET dept_name='$data[dept_name]', dom='$data[dom]', publication_status='$data[publication_status]' WHERE dept_id='$data[dept_id]'";
        if (mysqli_query($db_connect, $sql)) {
            $_SESSION['message'] = "Department information updated successfully";
            header('Location: manage_department.php');
        }else{
            die('Query Problem' . mysqli_error($db_connect));
        }
    }
	
	public function delete_dept_data($id)
	{
        $db_connect = $this->__construct();
		//$sql = "DELETE FROM tbl_dept WHERE dept_id='$id'";
        $sql="UPDATE tbl_dept SET deletion_status=0 WHERE dept_id='$id' ";
        if (mysqli_query($db_connect, $sql)) {
            $message="Delete Successfully";
            return $message;
        } else {
            die('Query Problem' . mysqli_error($db_connect));
        }
    }
	
	public function save_teacher_info($data)
	{
		$db_connect=$this->__construct();
		$sql = 'SELECT tea_office_id FROM tbl_teacher WHERE tea_office_id="'.$data[tea_office_id].'" ';
		if(mysqli_num_rows(mysqli_query($db_connect, $sql)) == 0)
		{
			$directory='uploaded_image/';
			$target_file=$directory.$_FILES['image']['name'];
			$file_type=pathinfo($target_file, PATHINFO_EXTENSION);
			$file_size=$_FILES['image']['size'];
			$check=getimagesize($_FILES['image']['tmp_name']);
			if($check) {
				if(file_exists($target_file)) {
					echo 'This file is already exists. please try new one';
				} else {
					if($file_size>10000000) {
						echo 'File is too large. please try new one';
					} else {
						if($file_type !='jpg' && $file_type != 'png') {
							echo 'File type is not valid. please try new one';
						}else
						{
							move_uploaded_file($_FILES['image']['tmp_name'], $target_file);
							$query="INSERT INTO tbl_teacher(tea_office_id,tea_name,dept_id,father_name,mother_name,contact,email,address,dob,gender,image,doc,dom,acceptable_status) VALUES('$data[tea_office_id]', '$data[tea_name]', '$data[dept_id]', '$data[father_name]', '$data[mother_name]', '$data[contact]', '$data[email]', '$data[address]', '$data[dob]', '$data[gender]', '$target_file', '$data[doc]', '$data[doc]', '$data[acceptable_status]')";
							if (mysqli_query($db_connect, $query)){
								$message="Add Teacher Successfully";
								return $message;
							}
							else{
								die('Query Problem' . mysqli_error($db_connect));
							}
						}
					}
				}
			} 
			else{
			   $message= "This is not an image";
			   return $message;
			}
		}
		else{
			$message="This ID already exist. Please try another one";
			return $message;
		}
	}
	
	public function select_all_teacher()
	{
		$db_connect=$this->__construct();
		$sql="SELECT t.*, d.dept_name FROM tbl_teacher as t,tbl_dept as d WHERE t.dept_id=d.dept_id AND t.deletion_status=1";
		if(mysqli_query($db_connect, $sql)){
            $query_result = mysqli_query($db_connect, $sql);
            return $query_result;
        }
		else{
            die('Query problem' . mysqli_error($db_connect));
        }
	}
	
	public function select_teacher_info_by_tea_id($tea_id)
	{
		$db_connect=$this->__construct();
		$sql="SELECT t.*, d.dept_name FROM tbl_teacher as t,tbl_dept as d WHERE t.dept_id=d.dept_id AND tea_id='$tea_id'";
		if(mysqli_query($db_connect, $sql)){
            $query_result = mysqli_query($db_connect, $sql);
            return $query_result;
        }
		else{
            die('Query problem' . mysqli_error($db_connect));
        }
	}
	
	public function update_tea_info($data)
	{
        $db_connect = $this->__construct();
		if(!$_FILES['image']['name'] && !$data['dept_id'])
		{
			$sql = "UPDATE tbl_teacher SET tea_name='$data[tea_name]', father_name='$data[father_name]', mother_name='$data[mother_name]', contact='$data[contact]', email='$data[email]', address='$data[address]', gender='$data[gender]', doc='$data[doc]', dom='$data[dom]', acceptable_status='$data[acceptable_status]' WHERE tea_id='$data[tea_id]'";
			if (mysqli_query($db_connect, $sql)) {
				$_SESSION['message'] = "Teacher information updated successfully";
				header('Location: manage_teacher.php');
			}else{
				die('Query Problem' . mysqli_error($db_connect));
			}
		}
		else if($_FILES['image']['name'])
		{
			$directory='uploaded_image/';
			$target_file=$directory.$_FILES['image']['name'];
			$file_type=pathinfo($target_file, PATHINFO_EXTENSION);
			$file_size=$_FILES['image']['size'];
			$check=getimagesize($_FILES['image']['tmp_name']);
			if($check)
			{
				if(file_exists($target_file)){
					echo 'This file is already exists. please try new one';
				}else{
					if($file_size>10000000){
						echo 'File is too large. please try new one';
					}else{
						if($file_type !='jpg' && $file_type != 'png') {
							echo 'File type is not valid. please try new one';
						}else{
							move_uploaded_file($_FILES['image']['tmp_name'], $target_file);
							$sql = "UPDATE tbl_teacher SET tea_name='$data[tea_name]', father_name='$data[father_name]', mother_name='$data[mother_name]', image='$target_file', contact='$data[contact]', email='$data[email]', address='$data[address]', gender='$data[gender]', doc='$data[doc]', dom='$data[dom]', acceptable_status='$data[acceptable_status]' WHERE tea_id='$data[tea_id]'";
							if (mysqli_query($db_connect, $sql)) {
								$_SESSION['message'] = "Teacher information updated successfully";
								header('Location: manage_teacher.php');
							}else{
								die('Query Problem' . mysqli_error($db_connect));
							}
						}
					}
				}
			} 
			else{
			   $message= "This is not an image";
			   return $message;
			}
		}
		else if($data['dept_id'])
		{
			$sql = "UPDATE tbl_teacher SET tea_name='$data[tea_name]', father_name='$data[father_name]', mother_name='$data[mother_name]', dept_id='$data[dept_id]', contact='$data[contact]', email='$data[email]', address='$data[address]', gender='$data[gender]', doc='$data[doc]', dom='$data[dom]', acceptable_status='$data[acceptable_status]' WHERE tea_id='$data[tea_id]'";
			if (mysqli_query($db_connect, $sql)) {
				$_SESSION['message'] = "Teacher information updated successfully";
				header('Location: manage_teacher.php');
			}else{
				die('Query Problem' . mysqli_error($db_connect));
			}
		}
		else
		{
			$directory='uploaded_image/';
			$target_file=$directory.$_FILES['image']['name'];
			$file_type=pathinfo($target_file, PATHINFO_EXTENSION);
			$file_size=$_FILES['image']['size'];
			$check=getimagesize($_FILES['image']['tmp_name']);
			if($check)
			{
				if(file_exists($target_file)){
					echo 'This file is already exists. please try new one';
				}else{
					if($file_size>10000000){
						echo 'File is too large. please try new one';
					}else{
						if($file_type !='jpg' && $file_type != 'png') {
							echo 'File type is not valid. please try new one';
						}else{
							move_uploaded_file($_FILES['image']['tmp_name'], $target_file);
							$sql = "UPDATE tbl_teacher SET tea_name='$data[tea_name]', father_name='$data[father_name]', mother_name='$data[mother_name]', dept_id='$data[dept_id]', image='$target_file', contact='$data[contact]', email='$data[email]', address='$data[address]', gender='$data[gender]', doc='$data[doc]', dom='$data[dom]', acceptable_status='$data[acceptable_status]' WHERE tea_id='$data[tea_id]'";
							if (mysqli_query($db_connect, $sql)) {
								$_SESSION['message'] = "Teacher information updated successfully";
								header('Location: manage_teacher.php');
							}else{
								die('Query Problem' . mysqli_error($db_connect));
							}
						}
					}
				}
			} 
			else{
			   $message= "This is not an image";
			   return $message;
			}
		}
    }
	
	public function delete_tea_info($id)
	{
        $db_connect = $this->__construct();
		//$sql = "DELETE FROM tbl_teacher WHERE tea_id='$id'";
        $sql="UPDATE tbl_teacher SET deletion_status=0 WHERE tea_id='$id' ";
        if (mysqli_query($db_connect, $sql)) {
            $message="Delete Successfully";
            return $message;
        } else {
            die('Query Problem' . mysqli_error($db_connect));
        }
    }
	
	public function save_news_category($data)
	{
        $db_connect = $this->__construct();
        $sql="INSERT INTO tbl_news_category (news_cat_name, doc, dom, publication_status) VALUES ('$data[news_cat_name]', '$data[doc]', '$data[doc]', '$data[publication_status]')";
        if(mysqli_query($db_connect, $sql)){
            $message="News Category Cteated successfully";
            return $message;
        }
		else
		{
            die('Query problem' . mysqli_error($db_connect));
        }
    }
	
	public function select_all_news_category()
	{
		$db_connect=$this->__construct();
		$sql="SELECT * FROM tbl_news_category WHERE deletion_status=1";
		if(mysqli_query($db_connect, $sql)){
            $query_result = mysqli_query($db_connect, $sql);
            return $query_result;
        }
		else{
            die('Query problem' . mysqli_error($db_connect));
        }
	}
	
	public function save_news($data)
	{
		$db_connect = $this->__construct();
		$directory='uploaded_image/';
		$target_file=$directory.$_FILES['image']['name'];
		$file_type=pathinfo($target_file, PATHINFO_EXTENSION);
		$file_size=$_FILES['image']['size'];
		$check=getimagesize($_FILES['image']['tmp_name']);
		if($check) {
			if(file_exists($target_file)) {
				echo 'This file is already exists. please try new one';
			} else {
				if($file_size>10000000) {
					echo 'File is too large. please try new one';
				} else {
					if($file_type !='jpg' && $file_type != 'png') {
						echo 'File type is not valid. please try new one';
					} else{
						move_uploaded_file($_FILES['image']['tmp_name'], $target_file);
						$sql="INSERT INTO tbl_news(news_title, news_cat_id, image, news_details, doc, dom, publication_status) VALUES ('$data[news_title]', '$data[news_cat_id]', '$target_file', '$data[news_details]', '$data[doc]', '$data[doc]', '$data[publication_status]' )";
						if (mysqli_query($db_connect, $sql) ) {
							$message="News saved successfully";
							return $message;
						} else {
							die('Query problem'.  mysqli_error($db_connect) );
						}
					}
				}
			}
		} 
		else{
		   $message= "This is not an image";
		   return $message;
		}
    }
	
	public function select_all_news()
	{
		$db_connect=$this->__construct();
		$sql="SELECT n.*, c.news_cat_name FROM tbl_news as n, tbl_news_category as c WHERE n.news_cat_id=c.news_cat_id AND n.deletion_status=1";
		if(mysqli_query($db_connect, $sql)){
            $query_result = mysqli_query($db_connect, $sql);
            return $query_result;
        }
		else{
            die('Query problem' . mysqli_error($db_connect));
        }
	}
	
	public function show_news_info($news_id)
	{
		$db_connect=$this->__construct();
		$sql="SELECT * FROM tbl_news WHERE news_id='$news_id' ";
		if(mysqli_query($db_connect, $sql)){
		   $query_result=mysqli_query($db_connect, $sql);
		   return $query_result;
		}else{
			die('Query problem'.  mysqli_error($db_connect) );
		}
	}
	
	public function update_news_info($data)
	{
        $db_connect = $this->__construct();
		if(!$_FILES['image']['name'])
		{
			$sql = "UPDATE tbl_news SET news_title='$data[news_title]', news_details='$data[news_details]', dom='$data[dom]', publication_status='$data[publication_status]' WHERE news_id='$data[news_id]'";
			if (mysqli_query($db_connect, $sql)) {
				$_SESSION['message'] = "News information updated successfully";
				header('Location: manage_news.php');
			}else{
				die('Query Problem' . mysqli_error($db_connect));
			}
		}
		else
		{
			$directory='uploaded_image/';
			$target_file=$directory.$_FILES['image']['name'];
			$file_type=pathinfo($target_file, PATHINFO_EXTENSION);
			$file_size=$_FILES['image']['size'];
			$check=getimagesize($_FILES['image']['tmp_name']);
			if($check){
				if(file_exists($target_file)){
					echo 'This file is already exists. please try new one';
				}else{
					if($file_size>10000000){
						echo 'File is too large. please try new one';
					}else{
						if($file_type !='jpg' && $file_type != 'png') {
							echo 'File type is not valid. please try new one';
						}else{
							move_uploaded_file($_FILES['image']['tmp_name'], $target_file);
							$sql = "UPDATE tbl_news SET news_title='$data[news_title]', image='$target_file', news_details='$data[news_details]', dom='$data[dom]', publication_status='$data[publication_status]' WHERE news_id='$data[news_id]'";
							if (mysqli_query($db_connect, $sql)) {
								$_SESSION['message'] = "News information updated successfully with image";
								header('Location: manage_news.php');
							}else{
								die('Query Problem' . mysqli_error($db_connect));
							}
						}
					}
				}
			} 
			else{
			   $message= "This is not an image";
			   return $message;
			}
		}
    }
	
	public function delete_news($id)
	{
        $db_connect = $this->__construct();
		//$sql = "DELETE FROM tbl_news WHERE news_id='$id'";
        $sql="UPDATE tbl_news SET deletion_status=0 WHERE news_id='$id' ";
        if (mysqli_query($db_connect, $sql)) {
            $message="Delete Successfully";
            return $message;
        } else {
            die('Query Problem' . mysqli_error($db_connect));
        }
    }
	
	public function select_all_mails()
	{
		$db_connect=$this->__construct();
		$sql="SELECT * FROM tbl_contact WHERE deletion_status=1 ORDER BY cnt_id DESC";
		if(mysqli_query($db_connect, $sql)){
            $query_result = mysqli_query($db_connect, $sql);
            return $query_result;
        }
		else{
            die('Query problem' . mysqli_error($db_connect));
        }
	}
	
	public function show_contact_info_by_id($cnt_id)
	{
		$db_connect=$this->__construct();
		$sql="SELECT * FROM tbl_contact WHERE cnt_id='$cnt_id'";
		if(mysqli_query($db_connect, $sql)){
            $query_result = mysqli_query($db_connect, $sql);
            return $query_result;
        }
		else{
            die('Query problem' . mysqli_error($db_connect));
        }
	}
	
	public function update_contact_info($data)
	{
        $db_connect = $this->__construct();
        $sql = "UPDATE tbl_contact SET message='$data[message]', acceptable_status='$data[acceptable_status]' WHERE cnt_id='$data[cnt_id]'";
        if (mysqli_query($db_connect, $sql)) {
            $_SESSION['message'] = "Email has been read successfully";
            header('Location: manage_contact.php');
        }else{
            die('Query Problem' . mysqli_error($db_connect));
        }
    }
	
	public function delete_contact($id)
	{
        $db_connect = $this->__construct();
		//$sql = "DELETE FROM tbl_contact WHERE cnt_id='$id'";
        $sql="UPDATE tbl_contact SET deletion_status=0 WHERE cnt_id='$id' ";
        if (mysqli_query($db_connect, $sql)) {
            $message="Delete Successfully";
            return $message;
        } else {
            die('Query Problem' . mysqli_error($db_connect));
        }
    }
	
	public function select_all_published_batch()
	{
		$db_connect=$this->__construct();
		$sql="SELECT * FROM tbl_batch WHERE publication_status=1 AND deletion_status=1";
		if(mysqli_query($db_connect, $sql)){
            $query_result = mysqli_query($db_connect, $sql);
            return $query_result;
        }
		else{
            die('Query problem' . mysqli_error($db_connect));
        }
	}
	
	public function save_student_info($data)
	{
		$db_connect=$this->__construct();
		$sql = 'SELECT stu_class_id FROM tbl_student WHERE stu_class_id="'.$data[stu_class_id].'" ';
		if(mysqli_num_rows(mysqli_query($db_connect, $sql)) == 0)
		{
			$directory='uploaded_image/';
			$target_file=$directory.$_FILES['image']['name'];
			$file_type=pathinfo($target_file, PATHINFO_EXTENSION);
			$file_size=$_FILES['image']['size'];
			$check=getimagesize($_FILES['image']['tmp_name']);
			if($check) {
				if(file_exists($target_file)) {
					echo 'This file is already exists. please try new one';
				} else {
					if($file_size>10000000) {
						echo 'File is too large. please try new one';
					} else {
						if($file_type !='jpg' && $file_type != 'png') {
							echo 'File type is not valid. please try new one';
						}else
						{
							move_uploaded_file($_FILES['image']['tmp_name'], $target_file);
							$query="INSERT INTO tbl_student(stu_class_id,stu_name,batch_id,father_name,mother_name,contact,email,address,gender,image,doc,dom,acceptable_status) VALUES('$data[stu_class_id]', '$data[stu_name]', '$data[batch_id]', '$data[father_name]', '$data[mother_name]', '$data[contact]', '$data[email]', '$data[address]', '$data[gender]', '$target_file', '$data[doc]', '$data[doc]', '$data[acceptable_status]')";
							if (mysqli_query($db_connect, $query)){
								$message="Add Student Successfully";
								return $message;
							}
							else{
								die('Query Problem' . mysqli_error($db_connect));
							}
						}
					}
				}
			} 
			else{
			   $message= "This is not an image";
			   return $message;
			}
		}
		else{
			$message="This ID already exist. Please try another one";
			return $message;
		}
	}
	
	public function select_all_student()
	{
		$db_connect=$this->__construct();
		$sql="SELECT s.*,b.batch_name FROM tbl_student as s, tbl_batch as b WHERE s.batch_id=b.batch_id AND s.deletion_status=1";
		if(mysqli_query($db_connect, $sql)){
            $query_result = mysqli_query($db_connect, $sql);
            return $query_result;
        }
		else{
            die('Query problem' . mysqli_error($db_connect));
        }
	}
	
	public function select_student_info_by_stu_id($stu_id)
	{
		$db_connect=$this->__construct();
		$sql="SELECT s.*, b.batch_name FROM tbl_student as s, tbl_batch as b WHERE s.batch_id=b.batch_id AND s.stu_id='$stu_id'";
		if(mysqli_query($db_connect, $sql)){
            $query_result = mysqli_query($db_connect, $sql);
            return $query_result;
        }
		else{
            die('Query problem' . mysqli_error($db_connect));
        }
	}
	
	public function select_all_batch_info_2()
	{
		$db_connect=$this->__construct();
		$sql="SELECT * FROM tbl_batch WHERE deletion_status=1";
		if(mysqli_query($db_connect, $sql)){
            $query_result_1 = mysqli_query($db_connect, $sql);
            return $query_result_1;
        }
		else{
            die('Query problem' . mysqli_error($db_connect));
        }
	}
	
	public function update_stu_info($data)
	{
        $db_connect = $this->__construct();
		if(!$_FILES['image']['name'] && !$data['batch_id'])
		{
			$sql = "UPDATE tbl_student SET stu_name='$data[stu_name]', father_name='$data[father_name]', mother_name='$data[mother_name]', contact='$data[contact]', email='$data[email]', address='$data[address]', gender='$data[gender]', doc='$data[doc]', dom='$data[dom]', acceptable_status='$data[acceptable_status]' WHERE stu_id='$data[stu_id]'";
			if (mysqli_query($db_connect, $sql)) {
				$_SESSION['message'] = "Student information updated successfully";
				header('Location: manage_student.php');
			}else{
				die('Query Problem' . mysqli_error($db_connect));
			}
		}
		else if($_FILES['image']['name'])
		{
			$directory='uploaded_image/';
			$target_file=$directory.$_FILES['image']['name'];
			$file_type=pathinfo($target_file, PATHINFO_EXTENSION);
			$file_size=$_FILES['image']['size'];
			$check=getimagesize($_FILES['image']['tmp_name']);
			if($check)
			{
				if(file_exists($target_file)){
					echo 'This file is already exists. please try new one';
				}else{
					if($file_size>10000000){
						echo 'File is too large. please try new one';
					}else{
						if($file_type !='jpg' && $file_type != 'png') {
							echo 'File type is not valid. please try new one';
						}else{
							move_uploaded_file($_FILES['image']['tmp_name'], $target_file);
							$sql = "UPDATE tbl_student SET stu_name='$data[stu_name]', father_name='$data[father_name]', mother_name='$data[mother_name]', image='$target_file',contact='$data[contact]', email='$data[email]', address='$data[address]', gender='$data[gender]', doc='$data[doc]', dom='$data[dom]', acceptable_status='$data[acceptable_status]' WHERE stu_id='$data[stu_id]'";
							if (mysqli_query($db_connect, $sql)) {
								$_SESSION['message'] = "Student information updated successfully";
								header('Location: manage_student.php');
							}else{
								die('Query Problem' . mysqli_error($db_connect));
							}
						}
					}
				}
			} 
			else{
			   $message= "This is not an image";
			   return $message;
			}
		}
		else if($data['batch_id'])
		{
			$sql = "UPDATE tbl_student SET stu_name='$data[stu_name]', father_name='$data[father_name]', mother_name='$data[mother_name]', batch_id='$data[batch_id]', contact='$data[contact]', email='$data[email]', address='$data[address]', gender='$data[gender]', doc='$data[doc]', dom='$data[dom]', acceptable_status='$data[acceptable_status]' WHERE stu_id='$data[stu_id]'";
			if (mysqli_query($db_connect, $sql)) {
				$_SESSION['message'] = "Student information updated successfully";
				header('Location: manage_student.php');
			}else{
				die('Query Problem' . mysqli_error($db_connect));
			}
		}
		else
		{
			$directory='uploaded_image/';
			$target_file=$directory.$_FILES['image']['name'];
			$file_type=pathinfo($target_file, PATHINFO_EXTENSION);
			$file_size=$_FILES['image']['size'];
			$check=getimagesize($_FILES['image']['tmp_name']);
			if($check)
			{
				if(file_exists($target_file)){
					echo 'This file is already exists. please try new one';
				}else{
					if($file_size>10000000){
						echo 'File is too large. please try new one';
					}else{
						if($file_type !='jpg' && $file_type != 'png') {
							echo 'File type is not valid. please try new one';
						}else{
							move_uploaded_file($_FILES['image']['tmp_name'], $target_file);
							$sql = "UPDATE tbl_student SET stu_name='$data[stu_name]', father_name='$data[father_name]', mother_name='$data[mother_name]', batch_id='$data[batch_id]', image='$target_file',contact='$data[contact]', email='$data[email]', address='$data[address]', gender='$data[gender]', doc='$data[doc]', dom='$data[dom]', acceptable_status='$data[acceptable_status]' WHERE stu_id='$data[stu_id]'";
							if (mysqli_query($db_connect, $sql)) {
								$_SESSION['message'] = "Student information updated successfully";
								header('Location: manage_student.php');
							}else{
								die('Query Problem' . mysqli_error($db_connect));
							}
						}
					}
				}
			} 
			else{
			   $message= "This is not an image";
			   return $message;
			}
		}
    }
	
	public function delete_stu_info($id)
	{
        $db_connect = $this->__construct();
		//$sql = "DELETE FROM tbl_student WHERE stu_id='$id'";
        $sql="UPDATE tbl_student SET deletion_status=0 WHERE stu_id='$id' ";
        if (mysqli_query($db_connect, $sql)) {
            $message="Delete Successfully";
            return $message;
        } else {
            die('Query Problem' . mysqli_error($db_connect));
        }
    }
	
	public function select_all_pending_student()
	{
		$db_connect=$this->__construct();
		$sql="SELECT * FROM tbl_student_pending WHERE deletion_status=1";
		if(mysqli_query($db_connect, $sql)){
            $query_result = mysqli_query($db_connect, $sql);
            return $query_result;
        }
		else{
            die('Query problem' . mysqli_error($db_connect));
        }
	}
	
	public function select_pend_student_info_by_id($stu_id_pend)
	{
		$db_connect=$this->__construct();
		$sql="SELECT * FROM tbl_student_pending WHERE stu_id_pend='$stu_id_pend'";
		if(mysqli_query($db_connect, $sql)){
            $query_result = mysqli_query($db_connect, $sql);
            return $query_result;
        }
		else{
            die('Query problem' . mysqli_error($db_connect));
        }
	}
	
	public function update_pending_stu_info($data)
	{
        $db_connect = $this->__construct();
		$sql = 'SELECT stu_class_id FROM tbl_student WHERE stu_class_id="'.$data[stu_class_id].'" ';
		if(mysqli_num_rows(mysqli_query($db_connect, $sql)) == 0)
		{
			$sql="INSERT INTO tbl_student (stu_class_id, stu_name, batch_id, father_name, mother_name, contact, email, address, gender, image, doc, dom, acceptable_status) VALUES ('$data[stu_class_id]', '$data[stu_name]', '$data[batch_id]', '$data[father_name]', '$data[mother_name]', '$data[contact]', '$data[email]', '$data[address]', '$data[gender]', '$data[image]', '$data[doc]', '$data[doc]', '$data[acceptable_status]')";
			
			$sql_1="UPDATE tbl_student_pending SET deletion_status=0 WHERE stu_id_pend='$data[stu_id_pend]' ";
			if (mysqli_query($db_connect, $sql_1)) {
				//header('Location: manage_student.php');
			} else {
				die('Query Problem' . mysqli_error($db_connect));
			}
			
			if(mysqli_query($db_connect, $sql)){
				$_SESSION['message'] = "Submitted Student Added Successfully";
				header('Location: manage_student.php');
			}
			else
			{
				die('Query problem' . mysqli_error($db_connect));
			}
		}else{
			$message="This ID already exist. Please try another one";
			return $message;
		}
    }
	
	public function delete_pending_stu_info($id)
	{
        $db_connect = $this->__construct();
        $sql="UPDATE tbl_student_pending SET deletion_status=0 WHERE stu_id_pend='$id' ";
        if (mysqli_query($db_connect, $sql)) {
            $query = mysqli_query($db_connect, $sql);
            return $query;
        } else {
            die('Query Problem' . mysqli_error($db_connect));
        }
    }
	
}