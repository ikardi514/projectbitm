<?php
	ob_start();

	session_start();

	require './obj_function.php';
	$obj_admin=new Admin();  

	$admin_id = $_SESSION['admin_id'];
	if ($admin_id == NULL) {
		header('Location: index.php');
	}



	if (isset($_GET['status'])) {
		$obj_admin->admin_logout();
	}
?>

<!DOCTYPE html>
<html lang="en">
    <head>

        <!-- start: Meta -->
        <meta charset="utf-8">
        <title>Admin-ASOS Coaching Center</title>
        <meta name="description" content="Bootstrap Metro Dashboard">
        <meta name="author" content="Dennis Ji">
        <meta name="keyword" content="Metro, Metro UI, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
        <!-- end: Meta -->

        <!-- start: Mobile Specific -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- end: Mobile Specific -->

        <!-- start: CSS -->
        <link id="bootstrap-style" href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
        <link id="base-style" href="css/style.css" rel="stylesheet">
        <link id="base-style-responsive" href="css/style-responsive.css" rel="stylesheet">
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,cyrillic-ext,latin-ext' rel='stylesheet' type='text/css'>
        <!-- end: CSS -->


        <link rel="shortcut icon" href="img/favicon.ico">
        <!-- end: Favicon -->

        <script>

            function checkDelete() {
//            alert('Test');
                var check = confirm('Are you sure to delete this?');
                if (check) {
                    return true;
                }
                else {
                    return false;
                }
            }
        </script>
    </head>
    <body>
        <div class="navbar">
            <div class="navbar-inner">
                <div class="container-fluid">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".top-nav.nav-collapse,.sidebar-nav.nav-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>
                    <a class="brand" href="admin_master.php"><span>ASOS Coaching Center</span></a>
                    <div class="nav-no-collapse header-nav">
                        <ul class="nav pull-right">
                            <!-- start: User Dropdown -->
                            <li class="dropdown">
                                <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
                                    <i class="halflings-icon white user"></i> 
                                    <?php echo $_SESSION['admin_name']; ?>
                                    <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="dropdown-menu-title">
                                        <span>Account Settings</span>
                                    </li>
                                    <li><a href="#"><i class="halflings-icon user"></i> Profile</a></li>
                                    <li><a href="?status=logout"><i class="halflings-icon off"></i> Logout</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <!-- end: Header Menu -->
                </div>
            </div>
        </div>
        <!-- start: Header -->
		
        <div class="container-fluid-full">
            <div class="row-fluid">
                <!-- start: Main Menu -->
                <div id="sidebar-left" class="span2">
                    <div class="nav-collapse sidebar-nav">
                        <ul class="nav nav-tabs nav-stacked main-menu">
                            <li><a href="admin_master.php"><i class="icon-bar-chart"></i><span class="hidden-tablet">Dashboard</span></a></li>	
                            <li><a href="add_admin.php"><i class="icon-lock"></i><span class="hidden-tablet">Add Admin</span></a></li>
                            <li><a href="manage_admin.php"><i class="icon-edit"></i><span class="hidden-tablet">Manage Admin Info</span></a></li>
                            <li><a href="add_batch.php"><i class="icon-eye-open"></i><span class="hidden-tablet">Add Batch</span></a></li>
							<li><a href="manage_batch.php"><i class="icon-edit"></i><span class="hidden-tablet">Manage Batch Info</span></a></li>
							<li><a href="add_department.php"><i class="icon-dashboard"></i><span class="hidden-tablet">Add Department</span></a></li>
							<li><a href="manage_department.php"><i class="icon-edit"></i><span class="hidden-tablet">Manage Department</span></a></li>
							<li><a href="add_teacher.php"><i class="icon-font"></i><span class="hidden-tablet">Add Teacher</span></a></li>
							<li><a href="manage_teacher.php"><i class="icon-edit"></i><span class="hidden-tablet">Manage Teacher</span></a></li>
							<li><a href="pending_student.php"><i class="icon-edit"></i><span class="hidden-tablet">Pending Student</span></a></li>
                            <li><a href="add_student.php"><i class="icon-font"></i><span class="hidden-tablet">Add Student</span></a></li>
                            <li><a href="manage_student.php"><i class="icon-edit"></i><span class="hidden-tablet">Manage Student Info</span></a></li>
                            <li><a href="add_news_category.php"><i class="icon-dashboard"></i><span class="hidden-tablet">Add News Category</span></a></li>
							<li><a href="manage_news_category.php"><i class="icon-dashboard"></i><span class="hidden-tablet">Manage News Category</span></a></li>
							<li><a href="add_news.php"><i class="icon-dashboard"></i><span class="hidden-tablet"> Add News</span></a></li>
                            <li><a href="manage_news.php"><i class="icon-edit"></i><span class="hidden-tablet"> Manage News</span></a></li> 
							<li><a href="manage_contact.php"><i class="icon-edit"></i><span class="hidden-tablet"> Manage Contact</span></a></li>                        
                        </ul>
                    </div>
                </div>
                <!-- end: Main Menu -->
                <noscript>
					<div class="alert alert-block span10">
						<h4 class="alert-heading">Warning!</h4>
						<p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
					</div>
                </noscript>
                <!-- start: Content -->
                <div id="content" class="span10">
                    <?php
						if(isset($pages)) 
						{
							if($pages=='add_admin'){
								include'./pages/add_admin_content.php';
							}
							else if ($pages=='manage_admin'){
								include './pages/manage_admin_content.php';
							}
							else if ($pages=='update_admin_info'){
								include './pages/update_admin_info_content.php';
							}
							else if ($pages=='add_batch'){
								include './pages/add_batch_content.php';
							}
							else if ($pages=='manage_batch'){
								include './pages/manage_batch_content.php';
							}
							else if ($pages=='update_batch_info'){
								include './pages/update_batch_info_content.php';
							}
							else if ($pages=='add_department'){
								include './pages/add_department_content.php';
							}
							else if ($pages=='manage_department'){
								include './pages/manage_department_content.php';
							}
							else if ($pages=='update_dept_info'){
								include './pages/update_dept_info_content.php';
							}
							else if ($pages=='add_teacher'){
								include './pages/add_teacher_content.php';
							}
							else if ($pages=='manage_teacher'){
								include './pages/manage_teacher_content.php';
							}
							else if ($pages=='view_teacher_info'){
								include './pages/view_teacher_info_content.php';
							}
							else if ($pages=='update_teacher_info'){
								include './pages/update_teacher_info_content.php';
							}
							else if ($pages=='pending_student'){
								include './pages/pending_student_content.php';
							}
							else if ($pages=='update_pending_student_info'){
								include './pages/update_pending_student_info_content.php';
							}
							else if ($pages=='add_student'){
								include './pages/add_student_content.php';
							}
							else if ($pages=='manage_student'){
								include './pages/manage_student_content.php';
							}
							else if ($pages=='view_student_info'){
								include './pages/view_student_info_content.php';
							}
							else if ($pages=='update_student_info'){
								include './pages/update_student_info_content.php';
							}
							else if ($pages=='add_news_category'){
								include './pages/add_news_category_content.php';
							}
							else if ($pages=='manage_news_category'){
								include './pages/manage_news_category_content.php';
							}
							else if ($pages=='add_news'){
								include './pages/add_news_content.php';
							}
							else if ($pages=='manage_news'){
								include './pages/manage_news_content.php';
							}
							else if ($pages=='update_news_info'){
								include './pages/update_news_info_content.php';
							}
							else if ($pages=='manage_contact'){
								include './pages/manage_contact_content.php';
							}
							else if ($pages=='update_contact_info'){
								include './pages/update_contact_info_content.php';
							}
						}
						else
						{
							include './pages/admin_home_content.php';
						}
                    ?>
                </div>
            </div><!--/#content.span10-->
        </div><!--/fluid-row-->
        <div class="modal hide fade" id="myModal">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h3>Settings</h3>
            </div>
            <div class="modal-body">
                <p>Here settings can be configured...</p>
            </div>
            <div class="modal-footer">
                <a href="#" class="btn" data-dismiss="modal">Close</a>
                <a href="#" class="btn btn-primary">Save changes</a>
            </div>
        </div>

        <div class="clearfix"></div>

        <footer>

            <p>
                <span style="text-align:left;float:left">&copy; 2016 <a href="http://jiji262.github.io/Bootstrap_Metro_Dashboard/" alt="Bootstrap_Metro_Dashboard">ASOS</a></span>

            </p>

        </footer>

        <!-- start: JavaScript-->

        <script src="js/jquery-1.9.1.min.js"></script>
        <script src="js/jquery-migrate-1.0.0.min.js"></script>

        <script src="js/jquery-ui-1.10.0.custom.min.js"></script>

        <script src="js/jquery.ui.touch-punch.js"></script>

        <script src="js/modernizr.js"></script>

        <script src="js/bootstrap.min.js"></script>

        <script src="js/jquery.cookie.js"></script>

        <script src='js/fullcalendar.min.js'></script>

        <script src='js/jquery.dataTables.min.js'></script>

        <script src="js/excanvas.js"></script>
        <script src="js/jquery.flot.js"></script>
        <script src="js/jquery.flot.pie.js"></script>
        <script src="js/jquery.flot.stack.js"></script>
        <script src="js/jquery.flot.resize.min.js"></script>

        <script src="js/jquery.chosen.min.js"></script>

        <script src="js/jquery.uniform.min.js"></script>

        <script src="js/jquery.cleditor.min.js"></script>

        <script src="js/jquery.noty.js"></script>

        <script src="js/jquery.elfinder.min.js"></script>

        <script src="js/jquery.raty.min.js"></script>

        <script src="js/jquery.iphone.toggle.js"></script>

        <script src="js/jquery.uploadify-3.1.min.js"></script>

        <script src="js/jquery.gritter.min.js"></script>

        <script src="js/jquery.imagesloaded.js"></script>

        <script src="js/jquery.masonry.min.js"></script>

        <script src="js/jquery.knob.modified.js"></script>

        <script src="js/jquery.sparkline.min.js"></script>

        <script src="js/counter.js"></script>

        <script src="js/retina.js"></script>

        <script src="js/custom.js"></script>
        <!-- end: JavaScript-->
    </body>
</html>
