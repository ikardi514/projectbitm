-- phpMyAdmin SQL Dump
-- version 3.1.3.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 03, 2016 at 08:31 AM
-- Server version: 5.1.33
-- PHP Version: 5.2.9

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `coaching_managemnt_basis`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin`
--

CREATE TABLE IF NOT EXISTS `tbl_admin` (
  `admin_id` int(2) NOT NULL AUTO_INCREMENT,
  `admin_name` varchar(100) NOT NULL,
  `email_address` varchar(32) NOT NULL,
  `password` varchar(256) NOT NULL,
  `access_level` tinyint(1) NOT NULL,
  `deletion_status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`admin_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tbl_admin`
--

INSERT INTO `tbl_admin` (`admin_id`, `admin_name`, `email_address`, `password`, `access_level`, `deletion_status`) VALUES
(1, 'Md Samian', 'mdsamian@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 1, 1),
(2, 'Istiaque Amin Samian', 'istiaqueamin@yahoo.com', 'e10adc3949ba59abbe56e057f20f883e', 2, 1),
(3, 'For Check', 'check@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_batch`
--

CREATE TABLE IF NOT EXISTS `tbl_batch` (
  `batch_id` int(5) NOT NULL AUTO_INCREMENT,
  `batch_name` varchar(50) NOT NULL,
  `class_name` varchar(50) NOT NULL,
  `starting_time` varchar(11) NOT NULL,
  `ending_time` varchar(11) NOT NULL,
  `publication_status` tinyint(1) NOT NULL,
  `doc` varchar(15) NOT NULL,
  `deletion_status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`batch_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `tbl_batch`
--

INSERT INTO `tbl_batch` (`batch_id`, `batch_name`, `class_name`, `starting_time`, `ending_time`, `publication_status`, `doc`, `deletion_status`) VALUES
(1, 'Batch 01', 'Class Three', '10:00 AM', '12:00 PM', 1, '04/29/2016', 1),
(2, 'Batch 02', 'Class Two', '03:00 AM', '05:00 PM', 1, '04/29/2016', 1),
(3, 'Batch 03', 'Class Five', '05.00 PM', '07.00 PM', 1, '04/29/2016', 1),
(4, 'Batch 04', 'Class One', '10:00 AM', '12:00 PM', 1, '04/29/2016', 1),
(5, 'Batch 05', 'Class Four', '3.00 PM', '5.00 PM', 1, '04/29/2016', 1),
(13, 'Batch 06', 'Class Three', '05:00 PM', '07:00 PM', 1, '04/29/2016', 1),
(14, 'check class updated', '', '3.00 PM', '5.00 PM', 1, '04/30/2016', 0),
(15, 'Batch 07', 'Class Four', '10:00 AM', '12:00 PM', 1, '05/03/2016', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_contact`
--

CREATE TABLE IF NOT EXISTS `tbl_contact` (
  `cnt_id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `subject` varchar(50) NOT NULL,
  `message` text NOT NULL,
  `acceptable_status` tinyint(1) NOT NULL DEFAULT '1',
  `deletion_status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`cnt_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `tbl_contact`
--

INSERT INTO `tbl_contact` (`cnt_id`, `name`, `email`, `subject`, `message`, `acceptable_status`, `deletion_status`) VALUES
(1, 'Samian', 'samiansyl90@gmail.com', 'Samian''s Subject', '<div style="text-align: justify;"><span style="font-size: 10pt;">message goes here. message goes here. message goes here. message goes here. message goes here. message goes here. message goes here. message goes here. message goes here.</span></div>', 1, 0),
(6, 'Unknown', 'unknown_01@gmail.com', 'Unknown', 'Unknowns message here. Unknowns message here. Unknowns message here. Unknowns message here. Unknowns message here. Unknowns message here. Unknowns message here. Unknowns message here. Unknowns message here. Unknowns message here.  ', 1, 1),
(2, 'Istiaque Amin', 'istiaqueamin@yahoo.com', 'Istiaque Subject', '<div style="text-align: justify;"><span style="font-size: 10pt;">Istiaque''s message goes here. Istiaque''s message goes here. Istiaque''s message goes here. Istiaque''s message goes here. Istiaque''s message goes here.</span></div>', 2, 1),
(8, 'Mustafizur', 'mustafiz@gmail.com', 'Subject of Mustafiz', '<div style="text-align: justify;"><span style="font-size: 10pt;">Mustafizur message goes here. Mustafizur message goes here. Mustafizur message goes here. Mustafizur message goes here. Mustafizur message goes here. Mustafizur message goes here. Mustafizur message goes here. Mustafizur message goes here. Mustafizur message goes here.</span></div>', 2, 1),
(3, 'Rahim Khan', 'rk@gmail.com', 'Rk Subject', '<div style="text-align: justify;"><span style="font-size: 10pt;">Rk''s message goes here. Rk''s message goes here. Rk''s message goes here. Rk''s message goes here. Rk''s message goes here. \r\nRk''s message goes here. Rk''s message goes here.</span></div>', 1, 1),
(7, 'Adnan', 'adnan@gmail.com', 'Subject Name', '<div style="text-align: justify;"><span style="font-size: 10pt;">Adnan''s message goes here. Adnan''s message goes here. Adnan''s message goes here. Adnan''s message goes here. Adnan''s message goes here. Adnan''s message goes here. Adnan''s message goes here. Adnan''s message goes here. Adnan''s message goes here.</span></div>', 2, 1),
(4, 'Saif', 'saif@gmail.com', 'Saif''s Subject', '<div style="text-align: justify;"><span style="font-size: 10pt;">Saif''s message goes here. Saif''s message goes here. Saif''s message goes here.</span></div>', 1, 0),
(5, 'Check', 'check@gmail.com', 'Check''s Subject', '<div style="text-align: justify;"><span style="font-size: 10pt;">Check''s message goes here. Check''s message goes here. Check''s message goes here.</span></div>', 2, 1),
(9, 'Rahim Khan', 'rk@gmail.com', 'Subject of Rahim', '<div style="text-align: justify;"><span style="font-size: 10pt;">Rahim Khan''s message goes here. Rahim Khan''s message goes here. Rahim Khan''s message goes here. Rahim Khan''s message goes here. Rahim Khan''s message goes here. Rahim Khan''s message goes here.</span></div>', 2, 1),
(10, 'Mahfuz', 'mahfuz@gmail.com', 'Mahfuz''s Subject', '<div style="text-align: justify;"><span style="font-size: 10pt;">Mahfuzs message goes here. Mahfuzs message goes here. Mahfuzs message goes here. Mahfuzs message goes here. Mahfuzs message goes here. Mahfuzs message goes here. Mahfuzs message goes here. Mahfuzs message goes here.</span></div>', 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_dept`
--

CREATE TABLE IF NOT EXISTS `tbl_dept` (
  `dept_id` int(5) NOT NULL AUTO_INCREMENT,
  `dept_name` varchar(20) NOT NULL,
  `doc` varchar(15) NOT NULL,
  `dom` varchar(15) NOT NULL,
  `publication_status` tinyint(1) NOT NULL,
  `deletion_status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`dept_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `tbl_dept`
--

INSERT INTO `tbl_dept` (`dept_id`, `dept_name`, `doc`, `dom`, `publication_status`, `deletion_status`) VALUES
(1, 'General', '04/25/2016', '04/25/2016', 1, 1),
(2, 'Science', '04/25/2016', '04/27/2016', 1, 1),
(3, 'Commerce', '04/25/2016', '04/25/2016', 1, 1),
(4, 'Humanities', '04/25/2016', '04/25/2016', 1, 1),
(5, 'Other', '04/25/2016', '04/26/2016', 1, 1),
(6, 'Checked Department', '04/26/2016', '04/28/2016', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_news`
--

CREATE TABLE IF NOT EXISTS `tbl_news` (
  `news_id` int(5) NOT NULL AUTO_INCREMENT,
  `news_title` varchar(100) NOT NULL,
  `news_cat_id` int(5) NOT NULL,
  `image` text NOT NULL,
  `news_details` text NOT NULL,
  `doc` varchar(50) NOT NULL,
  `dom` varchar(50) NOT NULL,
  `publication_status` tinyint(1) NOT NULL,
  `deletion_status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`news_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `tbl_news`
--

INSERT INTO `tbl_news` (`news_id`, `news_title`, `news_cat_id`, `image`, `news_details`, `doc`, `dom`, `publication_status`, `deletion_status`) VALUES
(1, 'First News Title', 5, 'uploaded_image/image_1.jpg', '<div style="text-align: justify;"><span style="font-size: 10pt;">news details goes here.&nbsp;</span><span style="font-size: 13.3333px;">news details goes here.&nbsp;</span><span style="font-size: 13.3333px;">news details goes here.&nbsp;</span><span style="font-size: 13.3333px;">news details goes here.&nbsp;</span><span style="font-size: 13.3333px;">news details goes here.&nbsp;</span><span style="font-size: 13.3333px;">news details goes here.&nbsp;</span><span style="font-size: 13.3333px;">news details goes here.&nbsp;</span><span style="font-size: 13.3333px;">news details goes here.&nbsp;</span><span style="font-size: 13.3333px;">news details goes here.&nbsp;</span><span style="font-size: 13.3333px;">news details goes here.&nbsp;</span><span style="font-size: 13.3333px;">news details goes here.&nbsp;</span><span style="font-size: 13.3333px;">news details goes here.&nbsp;</span><span style="font-size: 13.3333px;">news details goes here.&nbsp;</span><span style="font-size: 13.3333px;">news details goes here.&nbsp;</span><span style="font-size: 13.3333px;">news details goes here.&nbsp;</span><span style="font-size: 13.3333px;">news details goes here.&nbsp;</span><span style="font-size: 13.3333px;">news details goes here.&nbsp;</span><span style="font-size: 13.3333px;">news details goes here.&nbsp;</span><span style="font-size: 13.3333px;">news details goes here.&nbsp;</span><span style="font-size: 13.3333px;">news details goes here.&nbsp;</span><span style="font-size: 13.3333px;">news details goes here.&nbsp;</span><span style="font-size: 13.3333px;">news details goes here.&nbsp;</span><span style="font-size: 13.3333px;">news details goes here.&nbsp;</span><span style="font-size: 13.3333px;">news details goes here.&nbsp;</span><span style="font-size: 13.3333px;">news details goes here.&nbsp;</span><span style="font-size: 13.3333px;">news details goes here.&nbsp;</span><span style="font-size: 13.3333px;">news details goes here.&nbsp;</span></div><div style="text-align: justify;"><span style="font-size: 13.3333px;">news details goes here.&nbsp;</span><span style="font-size: 13.3333px;">news details goes here.&nbsp;</span><span style="font-size: 13.3333px;">news details goes here.&nbsp;</span><span style="font-size: 13.3333px;">news details goes here.&nbsp;</span><span style="font-size: 13.3333px;">news details goes here.&nbsp;</span><span style="font-size: 13.3333px;">news details goes here.&nbsp;</span><span style="font-size: 13.3333px;">news details goes here.&nbsp;</span><span style="font-size: 13.3333px;">news details goes here.&nbsp;</span><span style="font-size: 13.3333px;">news details goes here.&nbsp;</span><span style="font-size: 13.3333px;">news details goes here.&nbsp;</span><span style="font-size: 13.3333px;">news details goes here.&nbsp;</span></div><div style="text-align: justify;"><span style="font-size: 13.3333px;">news details goes here.&nbsp;</span><span style="font-size: 13.3333px;">news details goes here.&nbsp;</span><span style="font-size: 13.3333px;">news details goes here.&nbsp;</span><span style="font-size: 13.3333px;">news details goes here.&nbsp;</span><span style="font-size: 13.3333px;">news details goes here.&nbsp;</span></div><div style="text-align: justify;"><span style="font-size: 13.3333px;">news details goes here.&nbsp;</span><span style="font-size: 13.3333px;">news details goes here.&nbsp;</span><span style="font-size: 13.3333px;">news details goes here.&nbsp;</span><span style="font-size: 13.3333px;">news details goes here.&nbsp;</span><span style="font-size: 13.3333px;">news details goes here.&nbsp;</span></div>', '23/04/2016', '04/16/16', 1, 1),
(2, 'Second News', 5, 'uploaded_image/image_3.jpg', '<div style="text-align: justify;"><span style="font-size: 10pt;">second news goes here.&nbsp;</span><span style="font-size: 13.3333px;">second news goes here.&nbsp;</span><span style="font-size: 13.3333px;">second news goes here.&nbsp;</span><span style="font-size: 13.3333px;">second news goes here.&nbsp;</span><span style="font-size: 13.3333px;">second news goes here.&nbsp;</span><span style="font-size: 13.3333px;">second news goes here.&nbsp;</span><span style="font-size: 13.3333px;">second news goes here.&nbsp;</span></div><div style="text-align: justify;"><span style="font-size: 13.3333px;"><br></span></div><div style="text-align: justify;"><span style="font-size: 13.3333px;">second news goes here.&nbsp;</span><span style="font-size: 13.3333px;">second news goes here.&nbsp;</span><span style="font-size: 13.3333px;">second news goes here.&nbsp;</span><span style="font-size: 13.3333px;">second news goes here.&nbsp;</span><span style="font-size: 13.3333px;">second news goes here.&nbsp;</span></div><div style="text-align: justify;"><span style="font-size: 13.3333px; text-align: start;">second news goes here.&nbsp;</span><span style="font-size: 13.3333px;">second news goes here.&nbsp;</span><span style="font-size: 13.3333px;">second news goes here.&nbsp;</span><span style="font-size: 13.3333px;">second news goes here.&nbsp;</span><span style="font-size: 13.3333px;">second news goes here.&nbsp;</span><span style="font-size: 13.3333px;">second news goes here.&nbsp;</span><span style="font-size: 13.3333px;">second news goes here.&nbsp;</span><span style="font-size: 13.3333px;">second news goes here.&nbsp;</span><span style="font-size: 13.3333px;">second news goes here.&nbsp;</span><span style="font-size: 13.3333px;">second news goes here.&nbsp;</span></div><div style="text-align: justify;"><span style="font-size: 13.3333px;"><br></span></div><div style="text-align: justify;"><span style="font-size: 13.3333px; text-align: start;">second news goes here.&nbsp;</span><span style="font-size: 13.3333px;">second news goes here.&nbsp;</span><span style="font-size: 13.3333px;">second news goes here.&nbsp;</span><span style="font-size: 13.3333px;">second news goes here.&nbsp;</span><span style="font-size: 13.3333px;">second news goes here.&nbsp;</span><span style="font-size: 13.3333px;">second news goes here.&nbsp;</span><span style="font-size: 13.3333px;">second news goes here.&nbsp;</span><span style="font-size: 13.3333px;">second news goes here.&nbsp;</span><span style="font-size: 13.3333px;">second news goes here.&nbsp;</span><span style="font-size: 13.3333px;">second news goes here.&nbsp;</span><span style="font-size: 13.3333px;">second news goes here.&nbsp;</span></div><div style="text-align: justify;"><span style="font-size: 13.3333px;"><br></span></div><div style="text-align: justify;"><span style="font-size: 13.3333px; text-align: start;">second news goes here.&nbsp;</span><span style="font-size: 13.3333px;">second news goes here.&nbsp;</span><span style="font-size: 13.3333px;">second news goes here.&nbsp;</span><span style="font-size: 13.3333px;">second news goes here.&nbsp;</span><span style="font-size: 13.3333px;">second news goes here.&nbsp;</span><span style="font-size: 13.3333px;">second news goes here.&nbsp;</span><span style="font-size: 13.3333px;">second news goes here.&nbsp;</span></div>', '04/24/2016', '04/24/2016', 1, 1),
(4, 'Third News Title', 2, 'uploaded_image/1.jpg', '<div style="text-align: justify;"><font face="Arial, Verdana"><span style="font-size: 13.3333px;">Third News Details goes here.&nbsp;</span></font><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Third News Details goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Third News Details goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Third News Details goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Third News Details goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Third News Details goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Third News Details goes here.&nbsp;</span></div><div style="text-align: justify;"><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Third News Details goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Third News Details goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Third News Details goes here.&nbsp;</span></div><div style="text-align: justify;"><span style="font-family: Arial, Verdana; font-size: 13.3333px;"><br></span></div><div style="text-align: justify;"><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Third News Details goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Third News Details goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Third News Details goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Third News Details goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Third News Details goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Third News Details goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Third News Details goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Third News Details goes here.&nbsp;</span></div><div style="text-align: justify;"><span style="font-family: Arial, Verdana; font-size: 13.3333px;"><br></span></div><div style="text-align: justify;"><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Third News Details goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Third News Details goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Third News Details goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Third News Details goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Third News Details goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Third News Details goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Third News Details goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Third News Details goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Third News Details goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Third News Details goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Third News Details goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Third News Details goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Third News Details goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Third News Details goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Third News Details goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Third News Details goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Third News Details goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Third News Details goes here.&nbsp;</span></div><div style="text-align: justify;"><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Third News Details goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Third News Details goes here.&nbsp;</span></div><div style="text-align: justify;"><span style="font-family: Arial, Verdana; font-size: 13.3333px;"><br></span></div><div style="text-align: justify;"><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Third News Details goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Third News Details goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Third News Details goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Third News Details goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Third News Details goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Third News Details goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Third News Details goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Third News Details goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Third News Details goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Third News Details goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Third News Details goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Third News Details goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Third News Details goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Third News Details goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Third News Details goes here.&nbsp;</span></div>', '04/29/2016', '04/29/2016', 1, 1),
(5, 'Fourth News Title', 3, 'uploaded_image/3.jpg', '<div style="text-align: justify;"><font face="Arial, Verdana"><span style="font-size: 13.3333px;">Fourth News Details Goes here.&nbsp;</span></font><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fourth News Details Goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fourth News Details Goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fourth News Details Goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fourth News Details Goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fourth News Details Goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fourth News Details Goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fourth News Details Goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fourth News Details Goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fourth News Details Goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fourth News Details Goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fourth News Details Goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fourth News Details Goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fourth News Details Goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fourth News Details Goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fourth News Details Goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fourth News Details Goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fourth News Details Goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fourth News Details Goes here.&nbsp;</span></div><div style="text-align: justify;"><span style="font-family: Arial, Verdana; font-size: 13.3333px;"><br></span></div><div style="text-align: justify;"><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fourth News Details Goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fourth News Details Goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fourth News Details Goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fourth News Details Goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fourth News Details Goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fourth News Details Goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fourth News Details Goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fourth News Details Goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fourth News Details Goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fourth News Details Goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fourth News Details Goes here.&nbsp;</span></div><div style="text-align: justify;"><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fourth News Details Goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fourth News Details Goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fourth News Details Goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fourth News Details Goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fourth News Details Goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fourth News Details Goes here.&nbsp;</span></div><div style="text-align: justify;"><span style="font-family: Arial, Verdana; font-size: 13.3333px;"><br></span></div><div style="text-align: justify;"><span style="font-family: Arial, Verdana; font-size: 13.3333px;"><br></span></div><div style="text-align: justify;"><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fourth News Details Goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fourth News Details Goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fourth News Details Goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fourth News Details Goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fourth News Details Goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fourth News Details Goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fourth News Details Goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fourth News Details Goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fourth News Details Goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fourth News Details Goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fourth News Details Goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fourth News Details Goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fourth News Details Goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fourth News Details Goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fourth News Details Goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fourth News Details Goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fourth News Details Goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fourth News Details Goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fourth News Details Goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fourth News Details Goes here.&nbsp;</span></div><div style="text-align: justify;"><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fourth News Details Goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fourth News Details Goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fourth News Details Goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fourth News Details Goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fourth News Details Goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fourth News Details Goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fourth News Details Goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fourth News Details Goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fourth News Details Goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fourth News Details Goes here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fourth News Details Goes here.&nbsp;</span></div>', '04/29/2016', '04/29/2016', 1, 1),
(6, 'Fifth News Title', 6, 'uploaded_image/2.jpg', '<div style="text-align: justify;"><font face="Arial, Verdana"><span style="font-size: 13.3333px;">Fifth News Details Goes Here.&nbsp;</span></font><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fifth News Details Goes Here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fifth News Details Goes Here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fifth News Details Goes Here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fifth News Details Goes Here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fifth News Details Goes Here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fifth News Details Goes Here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fifth News Details Goes Here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fifth News Details Goes Here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fifth News Details Goes Here.&nbsp;</span></div><div style="text-align: justify;"><span style="font-family: Arial, Verdana; font-size: 13.3333px;"><br></span></div><div style="text-align: justify;"><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fifth News Details Goes Here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fifth News Details Goes Here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fifth News Details Goes Here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fifth News Details Goes Here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fifth News Details Goes Here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fifth News Details Goes Here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fifth News Details Goes Here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fifth News Details Goes Here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fifth News Details Goes Here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fifth News Details Goes Here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fifth News Details Goes Here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fifth News Details Goes Here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fifth News Details Goes Here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fifth News Details Goes Here.&nbsp;</span></div><div style="text-align: justify;"><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fifth News Details Goes Here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fifth News Details Goes Here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fifth News Details Goes Here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fifth News Details Goes Here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fifth News Details Goes Here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fifth News Details Goes Here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fifth News Details Goes Here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fifth News Details Goes Here.&nbsp;</span></div><div style="text-align: justify;"><span style="font-family: Arial, Verdana; font-size: 13.3333px;"><br></span></div><div style="text-align: justify;"><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fifth News Details Goes Here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fifth News Details Goes Here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fifth News Details Goes Here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fifth News Details Goes Here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fifth News Details Goes Here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fifth News Details Goes Here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fifth News Details Goes Here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fifth News Details Goes Here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fifth News Details Goes Here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fifth News Details Goes Here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fifth News Details Goes Here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fifth News Details Goes Here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fifth News Details Goes Here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fifth News Details Goes Here.&nbsp;</span></div><div style="text-align: justify;"><span style="font-family: Arial, Verdana; font-size: 13.3333px;"><br></span></div><div style="text-align: justify;"><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fifth News Details Goes Here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fifth News Details Goes Here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fifth News Details Goes Here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fifth News Details Goes Here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fifth News Details Goes Here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fifth News Details Goes Here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fifth News Details Goes Here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fifth News Details Goes Here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fifth News Details Goes Here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fifth News Details Goes Here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fifth News Details Goes Here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fifth News Details Goes Here.&nbsp;</span></div><div style="text-align: justify;"><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fifth News Details Goes Here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fifth News Details Goes Here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fifth News Details Goes Here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fifth News Details Goes Here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fifth News Details Goes Here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fifth News Details Goes Here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fifth News Details Goes Here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fifth News Details Goes Here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fifth News Details Goes Here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fifth News Details Goes Here.&nbsp;</span></div><div style="text-align: justify;"><span style="font-family: Arial, Verdana; font-size: 13.3333px;"><br></span></div><div style="text-align: justify;"><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fifth News Details Goes Here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fifth News Details Goes Here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fifth News Details Goes Here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fifth News Details Goes Here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fifth News Details Goes Here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fifth News Details Goes Here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fifth News Details Goes Here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fifth News Details Goes Here.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Fifth News Details Goes Here.&nbsp;</span></div>', '04/29/2016', '04/29/2016', 1, 1),
(8, 'Super 30 Founder Anand Kumar', 6, 'uploaded_image/maths_650x400_41446716406.jpg', '<div style="text-align: justify;"><font face="Arial, Verdana"><span style="font-size: 13.3333px;">Super 30 Founder Anand Kumar''s news.&nbsp;</span></font><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Super 30 Founder Anand Kumar''s news.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Super 30 Founder Anand Kumar''s news.&nbsp;</span></div><div style="text-align: justify;"><span style="font-family: Arial, Verdana; font-size: 13.3333px;"><br></span></div><div style="text-align: justify;"><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Super 30 Founder Anand Kumar''s news.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Super 30 Founder Anand Kumar''s news.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Super 30 Founder Anand Kumar''s news.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Super 30 Founder Anand Kumar''s news.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Super 30 Founder Anand Kumar''s news.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Super 30 Founder Anand Kumar''s news.&nbsp;</span></div><div style="text-align: justify;"><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Super 30 Founder Anand Kumar''s news.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Super 30 Founder Anand Kumar''s news.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Super 30 Founder Anand Kumar''s news.&nbsp;</span><span style="font-family: Arial, Verdana; font-size: 13.3333px;">Super 30 Founder Anand Kumar''s news.&nbsp;</span></div>', '04/27/2016', '04/27/2016', 1, 1),
(9, 'Farewell Speech for Student', 8, 'uploaded_image/maxresdefault.jpg', '<span style="color: rgb(51, 51, 51); font-family: Raleway, Arial, sans-serif; font-size: 14px; line-height: 26px; text-align: justify; background-color: rgb(255, 255, 255);">We have provided below some nice farewell speech for students studying in school. We have properly categorized the farewell speech for school students to be given by the teachers, principals, students themselves and their juniors in the school. These students farewell speech can be given to students whenever they left the college after completing their study. You can select any of the given farewell speeches for the students in school according to your need and requirement to speech on the farewell party.</span>', '04/30/2016', '04/30/2016', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_news_category`
--

CREATE TABLE IF NOT EXISTS `tbl_news_category` (
  `news_cat_id` int(5) NOT NULL AUTO_INCREMENT,
  `news_cat_name` varchar(50) NOT NULL,
  `doc` varchar(15) NOT NULL,
  `dom` varchar(15) NOT NULL,
  `publication_status` tinyint(1) NOT NULL,
  `deletion_status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`news_cat_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `tbl_news_category`
--

INSERT INTO `tbl_news_category` (`news_cat_id`, `news_cat_name`, `doc`, `dom`, `publication_status`, `deletion_status`) VALUES
(8, 'Entertainment', '04/29/2016', '04/29/2016', 1, 1),
(2, 'News of Class Ten', '04/26/2016', '04/26/2016', 1, 1),
(3, 'News of Class Eight', '04/28/2016', '04/28/2016', 1, 1),
(4, 'News of Class Six', '04/29/2016', '04/29/2016', 1, 1),
(5, 'Exam Related News', '04/28/2016', '04/28/2016', 1, 1),
(6, 'For all Students', '04/27/2016', '04/27/2016', 1, 1),
(7, 'Teacher Related News', '04/28/2016', '04/28/2016', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_student`
--

CREATE TABLE IF NOT EXISTS `tbl_student` (
  `stu_id` int(10) NOT NULL AUTO_INCREMENT,
  `stu_class_id` varchar(10) NOT NULL,
  `stu_name` varchar(50) NOT NULL,
  `batch_id` int(5) NOT NULL,
  `father_name` varchar(50) NOT NULL,
  `mother_name` varchar(50) NOT NULL,
  `contact` varchar(15) NOT NULL,
  `email` varchar(50) NOT NULL,
  `address` text NOT NULL,
  `gender` tinyint(1) NOT NULL,
  `image` text NOT NULL,
  `doc` varchar(11) NOT NULL,
  `dom` varchar(11) NOT NULL,
  `acceptable_status` tinyint(1) NOT NULL,
  `deletion_status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`stu_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `tbl_student`
--

INSERT INTO `tbl_student` (`stu_id`, `stu_class_id`, `stu_name`, `batch_id`, `father_name`, `mother_name`, `contact`, `email`, `address`, `gender`, `image`, `doc`, `dom`, `acceptable_status`, `deletion_status`) VALUES
(1, '101118001', 'Istiaque Amin Samian', 1, 'Muhib Ahmed', 'Rowshan Akter', '01722313843', 'samiansyl90@gmail.com', 'Mirpur-10<div>Dhaka, Bangladesh</div>', 1, 'uploaded_image/pro_pic.jpg', '04/23/2016', '04/23/2016', 1, 1),
(2, '101118002', 'Mahbub Hussain Adnan', 1, 'Ahmed Hussain', 'Farzana', '01677013414', 'adnan@gmail.com', 'Hawapara,<div>Sylhet, Bangladesh</div>', 1, 'uploaded_image/adnan.jpg', '04/23/2016', '04/23/2016', 1, 1),
(3, '101118003', 'RK Khan', 1, 'Khan', 'Begum', '01712545658', 'rk@gmail.com', 'Dhaka, Bangladesh', 1, 'uploaded_image/rk.jpg', '04/23/2016', '04/23/2016', 1, 1),
(4, '101118004', 'Saif Saifur', 2, 'Rahman', 'Syeda', '01712328357', 'saif@gmail.com', 'Mirpur,<div>Dhaka, Bangladesh</div>', 1, 'uploaded_image/12308434_1283121838380260_6028581726995812741_n.jpg', '04/28/2016', '04/28/2016', 1, 1),
(9, '101118009', 'New Student update1', 4, 'New Student father update1', 'New Student Mother update1', '01832212123', 'mdsamian_update1@gmail.com', 'jxcvjfd1111111111', 2, 'uploaded_image/teacher.png', '04/29/2016', '04/16/16', 1, 1),
(17, '10118005', 'Mahi Tasnim', 13, 'Mahi Tasnim''s Father', 'Mahi Tasnim Mother', '01720000023', 'mahi_tasnim@gmail.com', 'Mahi Tasnim Address', 2, 'uploaded_image/Student exams 2-383x256.jpg', '04/30/2016', '04/30/2016', 1, 1),
(15, '101118007', 'Mustafiz Hussain', 5, 'Hussain', 'Husneyara', '01722212123', 'mustafiz@gmail.com', 'Mirpur-10\r\nDhaka, Bangladesh', 1, 'uploaded_image/add_staff.png', '04/16/16', '04/16/16', 1, 1),
(10, '101118010', 'Subroto Roy', 5, 'Subroto''s Father Name', 'Subroto''s Mother Name', '0172111113', 'sujoy_mitra146@yahoo.com', 'Sunamgonj', 1, 'uploaded_image/staff.png', '04/25/2016', '04/16/16', 1, 1),
(11, '101118011', 'Delete', 13, 'Delete Father', 'Delete Mother', '01722212123', 'samiansyl90@gmail.com', 'sdfgdsg', 1, 'uploaded_image/batch_in.png', '04/25/2016', '04/25/2016', 1, 0),
(12, '101118012', 'Check Pending Stu', 5, 'Check Pending Father', 'Check Pending Mother', '01720000023', 'check@gmail.com', 'Check Pending Address', 2, 'uploaded_image/student.jpg', '04/29/2016', '04/29/2016', 1, 1),
(13, '101118013', 'Sumit Roy', 4, 'Father Check', 'Mother Check', '0172111113', 'sumit@gmail.com', 'Sylhet, Bangladesh', 1, 'uploaded_image/contact.png', '04/29/2016', '04/29/2016', 1, 1),
(14, '101118014', 'Jubayda Tasnim', 3, 'Tasnim''s Father', 'Tasnim''s Mother', '01722212123', 'tasnim@gmail.com', 'Tasnim''s Address', 2, 'uploaded_image/loan-obligations.jpg', '04/29/2016', '04/29/2016', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_student_pending`
--

CREATE TABLE IF NOT EXISTS `tbl_student_pending` (
  `stu_id_pend` int(10) NOT NULL AUTO_INCREMENT,
  `stu_name` varchar(50) NOT NULL,
  `interest_class` varchar(50) NOT NULL,
  `father_name` varchar(50) NOT NULL,
  `mother_name` varchar(50) NOT NULL,
  `contact` varchar(15) NOT NULL,
  `email` varchar(100) NOT NULL,
  `address` text NOT NULL,
  `gender` tinyint(1) NOT NULL,
  `dob` varchar(15) NOT NULL,
  `image` text NOT NULL,
  `current_institute` varchar(100) NOT NULL,
  `doc` varchar(15) NOT NULL,
  `deletion_status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`stu_id_pend`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `tbl_student_pending`
--

INSERT INTO `tbl_student_pending` (`stu_id_pend`, `stu_name`, `interest_class`, `father_name`, `mother_name`, `contact`, `email`, `address`, `gender`, `dob`, `image`, `current_institute`, `doc`, `deletion_status`) VALUES
(1, 'Mustafiz Hussain', 'Class Five', 'Hussain', 'Husneyara', '01722212123', 'mustafiz@gmail.com', 'Mirpur-10\r\nDhaka, Bangladesh', 1, '1993-05-13', './admin/uploaded_image/add_staff.png', 'Mirpur School', '2016-04-25', 0),
(2, 'Sumit Roy', 'Class Four', 'Father Check', 'Mother Check', '0172111113', 'sumit@gmail.com', 'Sylhet, Bangladesh', 1, '2016-03-18', './admin/uploaded_image/contact.png', 'Sylhet School', '2016-04-25', 0),
(3, 'Check Pending Stu', 'Class Five', 'Check Pending Father', 'Check Pending Mother', '01720000023', 'check@gmail.com', 'Check Pending Address', 2, '1990-02-02', './admin/uploaded_image/student.jpg', 'Check Pending School', '2016-04-28', 0),
(4, 'Jubayda Tasnim', 'Class Three', 'Tasnim''s Father', 'Tasnim''s Mother', '01722212123', 'tasnim@gmail.com', 'Tasnim''s Address', 2, '1990-08-17', './admin/uploaded_image/loan-obligations.jpg', 'Tasnim''s Current School', '2016-04-28', 0),
(5, 'Mahi Tasnim', 'Class Six', 'Mahi Tasnim''s Father', 'Mahi Tasnim Mother', '01720000023', 'mahi_tasnim@gmail.com', 'Mahi Tasnim Address', 2, '1988-04-28', './admin/uploaded_image/Student exams 2-383x256.jpg', 'Mahi Tasnim Current School', '2016-04-28', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_teacher`
--

CREATE TABLE IF NOT EXISTS `tbl_teacher` (
  `tea_id` int(5) NOT NULL AUTO_INCREMENT,
  `tea_office_id` int(15) NOT NULL,
  `tea_name` varchar(50) NOT NULL,
  `dept_id` int(5) NOT NULL,
  `father_name` varchar(50) NOT NULL,
  `mother_name` varchar(50) NOT NULL,
  `contact` varchar(15) NOT NULL,
  `email` varchar(50) NOT NULL,
  `address` text NOT NULL,
  `dob` varchar(15) NOT NULL,
  `gender` tinyint(1) NOT NULL,
  `image` text NOT NULL,
  `doc` varchar(15) NOT NULL,
  `dom` varchar(15) NOT NULL,
  `acceptable_status` tinyint(1) NOT NULL,
  `deletion_status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`tea_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `tbl_teacher`
--

INSERT INTO `tbl_teacher` (`tea_id`, `tea_office_id`, `tea_name`, `dept_id`, `father_name`, `mother_name`, `contact`, `email`, `address`, `dob`, `gender`, `image`, `doc`, `dom`, `acceptable_status`, `deletion_status`) VALUES
(1, 101, 'Sabir Ismail', 2, 'Ismail Hussain', 'Jamila', '01753122545', 'sabir_01@gmail.com', 'Sylhet, Bangladesh', '04/21/1986', 1, 'uploaded_image/10708494_10152720963499596_1447443403049698422_o.jpg', '04/26/2016', '04/26/2016', 1, 1),
(2, 102, 'Proshanta Roy', 2, 'Father Roy', 'Mother Roy', '01815212574', 'pranab.mitra146@gmail.com', 'Sylhet, Bangladesh', '07/14/1988', 1, 'uploaded_image/52855_155391897833971_3347010_o.jpg', '04/26/2016', '04/26/2016', 1, 1),
(3, 103, 'Farzana Akther', 1, 'Farzana Akther''s Father', 'Farzana Akther''s Mother', '017221111123', 'farzana_akther@gmail.com', 'Farzana Akther''s Address', '04/21/1989', 1, 'uploaded_image/img_141.jpg', '01/27/2016', '04/16/16', 1, 1),
(4, 104, 'Mousumi Begum', 3, 'Mousumi Begum''s Father', 'Mousumi Begum''s Mother', '01652144577', 'mmmm@gmail.com', 'Mousumi Begum''s Address', '04/29/1987', 2, 'uploaded_image/Loan-Forgiveness-For-Teachers.jpg', '05/19/2016', '04/16/16', 1, 1),
(5, 105, 'Enamul Hoque', 2, 'Enamul Hoque''s Father', 'Enamul Hoque''s Mother', '0172111113', 'enamul@gmail.com', '<font face="Arial, Verdana"><span style="font-size: 13.3333px;">Enamul Hoque''s Address</span></font>', '04/05/1988', 1, 'uploaded_image/Hopeful_student_1.jpg', '05/02/2016', '05/02/2016', 1, 1),
(6, 106, 'Jamal Hussain', 3, 'Jamal Hussain''s Father', 'Jamal Hussain''s Mother', '01632212123', 'jamal@yahoo.com', '<font face="Arial, Verdana"><span style="font-size: 13.3333px;">Jamal Hussain''s Address</span></font>', '04/16/1987', 1, 'uploaded_image/ReliaMax_male-student-holding-books.jpg', '01/01/2016', '01/01/2016', 1, 1),
(7, 108, 'Nazmin Akther', 4, 'Nazmin Akther''s Father', 'Nazmin Akther''s Mother', '01722212123', 'nazmin_akther@yahoo.com', '<font face="Arial, Verdana"><span style="font-size: 13.3333px;">Nazmin Akther''s Address</span></font>', '08/23/1986', 2, 'uploaded_image/school-student.png', '04/21/2016', '04/21/2016', 1, 1),
(8, 109, 'Tithy Begum', 1, 'Tithy Begum''s Father', 'Tithy Begum''s Mother', '0172111113', 'tithy_begum@gmail.com', '<font face="Arial, Verdana"><span style="font-size: 13.3333px;">Tithy Begum''s Address</span></font>', '04/30/1986', 2, 'uploaded_image/635903243401429014-1492924268_teachers.jpg', '01/31/2016', '01/31/2016', 1, 1),
(9, 110, 'Tuli Akther', 1, 'Tuli Akther''s Father', 'Tuli Akther''s Mother', '01625478541', 'tuli_012@gmail.com', '<font face="Arial, Verdana"><span style="font-size: 13.3333px;">Tuli Akther''s Address</span></font>', '09/13/1985', 2, 'uploaded_image/dichthuatsms-backgroundimage.jpg', '03/22/2016', '03/22/2016', 1, 1),
(10, 111, 'Saymon Khan', 4, 'Saymon Khan''s Father', 'Saymon Khan''s Mother', '01524874512', 'sk_01@gmail.com', '<font face="Arial, Verdana"><span style="font-size: 13.3333px;">Saymon Khan''s Address</span></font>', '08/17/1984', 1, 'uploaded_image/career-shift-how-to-become-a-substitute-teacher.jpg', '01/31/2016', '01/31/2016', 1, 1),
(11, 112, 'Mahbub Hoque', 2, 'Mahbub Hoque''s Father', 'Mahbub Hoque''s Mother', '01956457898', 'mahbub@yahoo.com', '<font face="Arial, Verdana"><span style="font-size: 13.3333px;">Mahbub Hoque''s Address</span></font>', '04/29/1982', 1, 'uploaded_image/MaleMathTeacher.jpg', '03/23/2016', '03/23/2016', 1, 1),
(12, 113, 'Sharmin Akther', 4, 'Sharmin Akther''s Father', 'Sharmin Akther''s Mother', '01524121478', 'sharmin@gmail.com', '<font face="Arial, Verdana"><span style="font-size: 13.3333px;">Sharmin Akther''s Address</span></font>', '11/07/1985', 2, 'uploaded_image/teacher-day.jpg', '02/25/2016', '02/25/2016', 1, 1),
(13, 114, 'Sayma Akther', 2, 'Sayma Akther''s Father', 'Sayma Akther''s Mother', '01754212325', 'saymaaaaa_akther@gmail.com', '<font face="Arial, Verdana"><span style="font-size: 13.3333px;">Sayma Akther''s Address</span></font>', '07/24/1987', 2, 'uploaded_image/young_woman_with_laptop_english_teacher_visual_stage.jpg', '02/27/2016', '02/27/2016', 1, 1),
(14, 115, 'Fatema Akther', 3, 'Fatema Akther''s Father', 'Fatema Akther''s Mother', '01756874512', 'fatema002@gmail.com', '<font face="Arial, Verdana"><span style="font-size: 13.3333px;">Fatema Akther''s Address</span></font>', '06/24/1987', 2, 'uploaded_image/Fotolia_52980884_XS.jpg', '01/30/2016', '01/30/2016', 1, 1);
