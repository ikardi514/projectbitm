<div id="tooplate_header_wrapper">
<div id="tooplate_header" class="wrapper">
	<div id="top_menu">
		<ul class="header_menu">
			<li><a href="index.php" class="selected">Home</a></li>
			<li><a href="about.php" >About</a></li>
			<li><a href="contact.php">Contact</a></li>
		</ul>
	</div>
	<div class="clear"></div>
	<a href="index.php" class="sitetitle"></a>
	<div id="tooplate_menu">
		<ul class="header_menu">
			<li><a href="registration.php">Registration</a></li>
			<li><a href="gallery.php">Galllery</a></li>
			<li><a href="news.php">News</a></li>
		</ul>
	</div>
	<div id="tooplate_slider">
		<div class="slider-wrapper theme-default">
			<div id="slider" class="nivoSlider">
				<img src="images/slider/1.jpg" alt="" title="" />
				<img src="images/slider/2.jpg" alt="" title="" />
				 <img src="images/slider/3.jpg" alt="" title="" />
				<img src="images/slider/4.jpg" alt="" title="" />                
			</div>
		</div>
		<script type="text/javascript" src="js/jquery-1.6.3.min.js"></script>
		<script type="text/javascript" src="js/jquery.nivo.slider.pack.js"></script>
		<script type="text/javascript">
		$(window).load(function() {
			$('#slider').nivoSlider({
			effect: 'random',
			controlNav: true, // 1,2,3... navigation
			directionNav: false,
			animSpeed: 800, // Slide transition speed
			pauseTime: 4000, // How long each slide will show
			});
		});
		</script>	
	</div> <!-- END of slider -->
</div> <!-- END of header -->
</div> <!-- END of tooplate_header_wrapper -->